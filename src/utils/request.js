import axios from "axios"
import {getToken, loginOut} from "./auth";


/**
 * 跨域解决方法
 * https://www.jianshu.com/p/ac9c58cee838
 * @type {string}
 */
axios.defaults.baseURL = "/api"
const instance = axios.create({
    baseURL: axios.defaults.baseURL,
    timeout: 15000
})

export const loginInstance = axios.create({
    baseURL: axios.defaults.baseURL,
    timeout: 15000
})


/**
 * 全局请求拦截,发送请求之前
 * 所有请求都需要带token
 */
instance.interceptors.request.use(function (config) {
    const token = getToken();
    if (token) {
        config.headers["Authorization"] = token['token_type'] + " " + token['access_token'];
    }
    return config;
}, function (error) {
    return Promise.reject(error);
})

/**
 * 全局响应拦截， 响应之后拦截
 */
instance.interceptors.response.use(function (response) {
    if (response.status === 401) {
        loginOut()
    }
    return response;
}, function (error) {
    if (error.response.status === 401) {
        loginOut()
    }
    return Promise.reject(error);
})

export function get(url, params) {
    return instance.get(url, {
        params
    });
}

export function post(url, data) {
    return instance.post(url, data);
}

export function patch(url, data) {
    return instance.patch(url, data);
}

export function put(url, data) {
    return instance.put(url, data);
}

export function del(url) {
    return instance.delete(url);
}