import {loginOutAndCleanToken} from "../services/login";
import {message} from "antd";
import {CONFIG} from "./config";

const getCurrentTime = () => {
    return new Date().getTime()
}

export const getToken = () => {
    let token = sessionStorage.getItem(CONFIG.KEY_LOGIN_TOKEN);
    if (token) {
        token = JSON.parse(token)
        let d = getCurrentTime();
        console.log("token::::current:", d, "    ", token.date, "  差：", (token.date - d))
        if (token.date < d) {
            clearToken(token).then()
            return null
        }
        return token;
    } else {
        return null
    }
}

export const setToken = (token) => {
    let t = getCurrentTime()
    // token.date = t + parseInt(token.expires_in);
    const expires = 60 * 60 * 1000 //设置1小时过期
    token.date = t + expires;
    sessionStorage.setItem(CONFIG.KEY_LOGIN_TOKEN, JSON.stringify(token));
}

export const isLogin = () => {
    let token = getToken();
    return token !== null;
}

export const loginOut = (props) => {
    const token = getToken()
    const res = clearToken(token);
    if (res) {
        props.history.push("/login")
    } else {
        message.destroy()
        message.error("退出接口异常！")
    }
}

const clearToken = async (token) => {
    sessionStorage.clear()
    let res = true
    if (token) {
        res = await loginOutAndCleanToken(token['access_token']).then(() => true).catch(() => false)
    }
    return res;
}

export const getMyMenus = () => {
    let myAdminMenu = sessionStorage.getItem(CONFIG.KEY_MY_ADMIN_MENU);
    if (myAdminMenu === null) {
        return []
    }
    return JSON.parse(myAdminMenu);
}

export const setMyMenus = (menu) => {
    sessionStorage.setItem(CONFIG.KEY_MY_ADMIN_MENU, JSON.stringify(menu.data))
}