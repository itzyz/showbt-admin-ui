export const CONFIG = {
    pageSize: 10,
    KEY_LOGIN_TOKEN: "login_token",
    KEY_MY_ADMIN_MENU: "my-admin-menu",
    KEY_PROJECT_LABEL_NAME: "project_label_name",
    KEY_MODULE_LABEL_NAME: "module_label_name",
    KEY_SERVER_LABEL_NAME: "server_label_name",
}