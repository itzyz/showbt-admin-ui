/**
 * 生成树型数据结构，创建路由专用
 * @param result
 * @param menu
 * @returns {*[]}
 */
export function createTreeData(result = [], dataList, icon = null) {
    dataList.forEach(m => {
        let obj = {title: m.name, value: m.id.toString(), key: m.id.toString(), type: m.type, icon: icon}
        if (m.children && m.children.length > 0) {
            let child = createTreeData([], m.children)
            obj.children = child
            result.push(obj)
        } else {
            result.push(obj)
        }
    })
    return result;
}

export function createTreeDataByDefault(dataList,
                                        defaultTitle = "默认选项",
                                        defaultValue = "-1",
                                        icon = null) {
    let treeResult = [];
    createTreeData(treeResult, dataList, icon);
    return [{title: defaultTitle, key: defaultValue, value: defaultValue, children: treeResult, type: "0", icon: icon}];
}


/**
 * 生成树型数据结构，创建左侧导航菜单专用
 * @param result
 * @param dataList
 * @param icon
 * @returns {*[]}
 */
export function createNavTreeData(result = [], dataList, icon = null) {
    dataList.forEach(m => {
        let obj = {name: m.name, key: m.id.toString(), type: m.type, path: m.path, icon: m.icon?m.icon:icon}
        if (m.children && m.children.length > 0) {
            let child = createNavTreeData([], m.children)
            obj.children = child
        }
        if (m.type === "0") {
            result.push(obj)
        }
    })
    return result;
}

export function createDepartTreeData(result = [], dataList, icon = null) {
    dataList.forEach(m => {
        let obj = {title: m.name, value: m.id, key: m.id, icon: m.icon?m.icon:icon}
        if (m.children && m.children.length > 0) {
            let child = createDepartTreeData([], m.children)
            obj.children = child
            result.push(obj)
        } else {
            result.push(obj)
        }
    })
    return result;
}

/**
 * 创建应用树型结构
 * @param result
 * @param dataList
 * @param icon
 * @returns {*[]}
 */
export function createAppTreeData(result = [], dataList, icon = null) {
    dataList.forEach(m => {
        let obj = {title: m.appName, value: m.id, key: m.id, icon: icon}
        if (m.children && m.children.length > 0) {
            let child = createAppTreeData([], m.children)
            obj.children = child
            result.push(obj)
        } else {
            result.push(obj)
        }
    })
    return result;
}

/**
 * 创建服务树形结构，用于应用授权
 * @param result
 * @param dataList
 * @param icon
 * @returns {*[]}
 */
export function createServerTreeData(result = [], dataList, icon = null) {
    dataList.forEach(m => {
        let title = ""
        let child = [];
        if (m.projectName) {
            title = m.projectName
            child = m.modules
        } else if (m.moduleName) {
            title = m.moduleName
            child = m.servers
        } else if (m['serverName']) {
            title = m['serverName']
        }

        let obj = {title: title, value: m.id, key: m.id, icon: icon}
        if (child && child.length > 0) {
            let children = createServerTreeData([], child)
            obj.children = children
            result.push(obj)
        } else {
            result.push(obj)
        }
    })
    return result;
}


/**
 * 扁平化的数据格式转换成树弄结构数据
 * 本方法用于获取第一级
 * @param treeData
 * @returns {[]}
 */
export const getFirstServerTree = (treeData) => {
    const result = []
    treeData.forEach(m => {
        if (m.pid === 0) {
            delete m.pid
            result.push(m)
        }
    })
    return result
}

/**
 * 扁平化的数据格式转换成树弄结构数据
 * 遍历第一级，从扁平数据表中找出第一级中的所有第下一级，再用递归从扁平表中找出所有下级
 * @param result 首次传入的是第一级
 * @param treeData
 * @returns {[]}
 */
export const getServerTree = (result = [], treeData) => {
    for (let i in result) {
        let p = result[i]
        for (let j in treeData) {
            let obj = treeData[j]
            if (obj.pid === p.key) {
                delete obj.pid
                if (p.children) {
                    p.children.push(obj)
                } else {
                    p.children = [obj]
                }
            }
        }
        if (p.children) {
            getServerTree(p.children, treeData)
        } else {
            p.children = null
        }
        if (p.children) {
            p.disabled = true
        }
    }
    return result
}