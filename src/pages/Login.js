import React, {useEffect, useState} from 'react'
import {Form, Input, Checkbox, Button, Card, message, Image, Space} from "antd";
import {UserOutlined, LockOutlined} from '@ant-design/icons';
import "./login.css"
import {setToken, setMyMenus} from "../utils/auth";
import {loginByUsername, validateCode} from "../services/login"
import getCurrentUsersMenu from "../services/upms/menu"

import logo from '../images/logo.png'

function Login(props) {
    const onFinish = values => {
        //当用户登录成功后，立即加载当前用的菜单，用于做路由渲染
        loginByUsername(values.username, values.password, values.code, randomStr).then(res => {
            if (res.status === 200 && res.data) {
                setToken(res.data)
                getCurrentUsersMenu().then(res0 => {
                    if (res0.status === 200 && res0.data.code === 200) {
                        setMyMenus(res0.data)
                    }
                    props.history.push("/admin")
                }).catch(err0 => {
                    message.destroy();
                    message.error("菜单获取失败！")
                })
            } else {
                getValidateCode();
                message.destroy();
                message.error(res.data.msg)
            }
        }).catch(err => {
            getValidateCode();
            message.destroy();
            message.error("用户名或者密码不正确！");
        })
    }

    const [imageUrl, setImageUrl] = useState("");
    const [randomStr, setRandomStr] = useState("12323");
    useEffect(() => {
        getValidateCode();
    }, [])

    const getValidateCode = () => {
        validateCode().then(res => {
            setImageUrl(res.data.data.image)
            setRandomStr(res.data.data.validateKey)
        }).catch(err => {
            message.destroy();
            message.error("获取验证码失败");
        });
    }

    return (
        <div className="login-form">

            <Card title={
                <div style={{textAlign:"center",  lineHeight:"55px"}}>
                    <img src={logo} style={{width: "50px", height: "50px", marginRight:"15px"}}></img>
                    <span style={{fontSize:"30px", height:"50px",verticalAlign:"middle" }}>网关管理系统</span>
                </div>
            }>
                <Form
                    name="normal_login"
                    initialValues={{remember: true, randomStr: randomStr}}
                    onFinish={onFinish}
                >
                    <Form.Item
                        name="username"
                        rules={[{required: true, message: '请输入用户名称!'}]}
                    >
                        <Input prefix={<UserOutlined className="site-form-item-icon"/>} placeholder="登录账号"/>
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[{required: true, message: '请输入密码!'}]}
                    >
                        <Input
                            prefix={<LockOutlined className="site-form-item-icon"/>}
                            type="password"
                            placeholder="账号密码"
                        />
                    </Form.Item>
                    <Form.Item style={{marginBottom: 0}}>
                        <Form.Item
                            name="code"
                            rules={[{required: true, message: "验证码不能为空！"}]}
                            style={{display: 'inline-block', width: 'calc(50% - 8px)'}}
                        >
                            <Input placeholder="验证码"/>
                        </Form.Item>
                        <Form.Item
                            style={{display: 'inline-block', width: 'calc(50% - 8px)', margin: '0 8px'}}
                        >
                            <img src={imageUrl} alt="" onClick={() => {
                                getValidateCode()
                            }}/>
                        </Form.Item>
                    </Form.Item>

                    <Form.Item>
                        <Form.Item name="remember" valuePropName="checked" noStyle>
                            <Checkbox>记录密码</Checkbox>
                        </Form.Item>
                        {/*<a className="login-form-forgot" href="">*/}
                        {/*    Forgot password*/}
                        {/*</a>*/}
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            登录
                        </Button>
                        {/*Or <a href="">register now!</a>*/}
                    </Form.Item>
                </Form>
            </Card>
        </div>
    )
}

export default Login
