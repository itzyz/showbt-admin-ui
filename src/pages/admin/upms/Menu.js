import React, {useEffect, useState} from 'react'
import {Card, Table, Button, message, Popconfirm} from 'antd';
import {deleteMenu, getMenuById, getMenuTree, saveOrUpdateMenu} from "../../../services/upms/menu";
import MenuEdit from "./MenuEdit";
import {createTreeDataByDefault} from "../../../utils/tools";

function MyMenu() {

    const [dataSource, setDataSource] = useState([])
    const [treeData, setTreeData] = useState([])
    const [visible, setVisible] = useState(false)
    const [confirmLoading, setConfirmLoading] = useState(false)
    const [currentDetail, setCurrentDetail] = useState({})
    const [expandedKeys, setExpandedKeys] = useState(["-1"])

    useEffect(() => {
        refreshTreeData();
    }, [])

    const columns = [
        {
            title: '菜单名称',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: '菜单类型',
            dataIndex: 'type',
            key: 'type',
        },
        {
            title: '菜单路径',
            dataIndex: 'path',
            key: 'path',
            width: '12%',
        },
        {
            title: '菜单图标',
            dataIndex: 'icon',
            key: 'icon',
        },
        {
            title: '组件路径',
            dataIndex: 'component',
            width: '30%',
            key: 'icon',
        },
        {
            title: "操作",
            render: (txt, record, index) => {
                return <>
                    <Button type="primary" size="small" onClick={() => showModalUpdate(record.id)}>修改</Button>
                    <Popconfirm title="确定删除菜单吗？"
                                onCancel={() => {
                                    message.info("删除操作已取消！")
                                }}
                                onConfirm={() => {
                                    console.log(record)
                                    deleteMenu(record.id).then(res => {
                                        message.info("删除成功！")
                                        refreshTreeData();
                                    }).catch(err => {
                                        message.error("删除失败！")
                                    })
                                }}>
                        <Button type="danger" size="small" style={{marginLeft: "1rem"}}>删除</Button>
                    </Popconfirm>
                </>
            }
        }
    ];

    const refreshTreeData = () => {
        getMenuTree().then(res => {
            let treeResult = createTreeDataByDefault(res.data.data)
            setDataSource(res.data.data)
            setTreeData(treeResult)
        }).catch(err => {
            message.destroy()
            message.error("获取菜单出错！")
        })
    }

    const save = (values) => {
        if (values.status) {
            values.status = 1
        } else {
            values.status = 0
        }
        saveOrUpdateMenu(values).then(res => {
            setVisible(false)
            refreshTreeData()
        }).catch(err => {
            message.error("菜单保存失败！！！");
        })

    }

    //点击新增按钮时触发此事件
    const showModalAdd = () => {
        //清空表单默认值
        setVisible(true)
        setCurrentDetail({})
    }

    //点击更新按钮时触发此事件
    const showModalUpdate = (id) => {
        getMenuById(id).then(res => {
            let data = res.data.data;
            data.parentId = data.parentId.toString()
            setCurrentDetail(data)
            setExpandedKeys([data.parentId])
            setVisible(true)
        }).catch(err => {
            message.info("获取菜单失败")
        })
    }

    // const {dataSource, treeData, currentDetail, visible, confirmLoading, expandedKeys} = this.state;
    return (
        <Card title="菜单列表" extra={<Button type="primary" onClick={showModalAdd}>新增</Button>}>
            <Table
                columns={columns}
                // rowSelection={{ ...rowSelection, checkStrictly }}
                dataSource={dataSource}
                rowKey={columns => columns.id}
            />
            <MenuEdit
                visible={visible}
                confirmLoading={confirmLoading}
                currentDetail={currentDetail}
                onCancel={() => {
                    setVisible(false)
                }} currentDetailData={{}}
                submitMap={(values) => {
                    save(values)
                }}
                treeData={treeData}
                expandedKeys={expandedKeys}
            ></MenuEdit>
        </Card>
    );
}

export default MyMenu