import {Modal, Input, Form, TreeSelect, Radio} from "antd";
import React, {useState} from "react";
import {MessageInstance as message} from "antd/lib/message";

const MenuEdit = (props) => {
    const {visible, confirmLoading, submitMap, onCancel, currentDetail, treeData, expandedKeys} = props

    console.log("menu=>treeData=>:::", treeData)

    const formRef = React.createRef();
    const handleOk = () => {
        formRef.current.validateFields().then(values => {
            submitMap(values)
        }).catch(err => {
            message.info("保存失败！！！")
        })
    }

    const [selectValue, setSelectValue] = useState(-1)

    const onChange = value => {
        setSelectValue(value)
    };

    return <Modal title="编辑菜单" visible={visible}
                  onOk={handleOk}
                  confirmLoading={confirmLoading}
                  onCancel={onCancel}
                  destroyOnClose={true}
                  maskClosable={false}
                  width="50%">
        <Form ref={formRef} initialValues={currentDetail}>
            <Form.Item hidden={true} name="menuId">
                <Input/>
            </Form.Item>
            <Form.Item label="上级菜单" name="parentId">
                <TreeSelect
                    style={{width: '100%'}}
                    value={selectValue}
                    dropdownStyle={{maxHeight: 400, overflow: 'auto'}}
                    treeData={treeData}
                    placeholder="请选择上级菜单"
                    treeDefaultExpandedKeys={expandedKeys}
                    onChange={onChange}
                />
            </Form.Item>
            <Form.Item label="菜单名称" name="name" rules={[{require: true, message: "请输入菜单名称"}]}>
                <Input/>
            </Form.Item>
            <Form.Item label="菜单类型" name="type">
                <Radio.Group name="type">
                    <Radio value="0">菜单</Radio>
                    <Radio value="1">功能</Radio>
                    <Radio value="2">子路由</Radio>
                </Radio.Group>
            </Form.Item>
            <Form.Item label="菜单权限" name="permission" rules={[{require: true, message: "请输入菜单权限"}]}>
                <Input/>
            </Form.Item>
            <Form.Item label="访问路径" name="path" rules={[{require: true, message: "请输入访问路径"}]}>
                <Input/>
            </Form.Item>
            <Form.Item label="图标" name="icon" rules={[{require: true, message: "请输入图标"}]}>
                <Input/>
            </Form.Item>
            <Form.Item label="渲染组件" name="component" rules={[{require: true, message: "请输入渲染组件"}]}>
                <Input/>
            </Form.Item>
        </Form>
    </Modal>
}

export default MenuEdit;