import {Modal, Input, Form, TreeSelect, Switch, Checkbox, message} from "antd";
import React, {useState} from "react";

const UserEdit = (props) => {
    const {visible, confirmLoading, submitMap, onCancel, currentDetail, treeData, roleList} = props

    const formRef = React.createRef();
    const handleOk = () => {
        formRef.current.validateFields().then(values => {
            submitMap(values)
        }).catch(err => {
            message.info("保存失败！！！")
        })
    }
    const [selectValue, setSelectValue] = useState(currentDetail.deptId)
    const onChange = value => {
        setSelectValue(value)
    };

    return <Modal title="编辑用户" visible={visible}
                  onOk={handleOk}
                  confirmLoading={confirmLoading}
                  onCancel={onCancel}
                  destroyOnClose={true}
                  maskClosable={false}
                  width="50%">
        <Form ref={formRef} initialValues={currentDetail}>
            <Form.Item hidden={true} name="userId">
                <Input/>
            </Form.Item>
            <Form.Item name="username" label="用户名称" rules={[{require: true, message: "请输入用户名称"}]}>
                <Input/>
            </Form.Item>
            <Form.Item name="password" label="用户密码">
                <Input.Password placeholder="不修改密码请置空！"/>
            </Form.Item>
            <Form.Item name="phone" label="手机号码" rules={[{require: true, message: "请输入手机号码"}]}>
                <Input/>
            </Form.Item>
            <Form.Item name="deptId" label="所在部门">
                <TreeSelect
                    style={{width: '100%'}}
                    value={selectValue}
                    dropdownStyle={{maxHeight: 400, overflow: 'auto'}}
                    treeData={treeData}
                    placeholder="请选择上级菜单"
                    treeDefaultExpandAll
                    onChange={onChange}
                />
            </Form.Item>
            <Form.Item name="lockFlag" label="锁定状态" valuePropName="checked">
                <Switch checkedChildren="已锁定" unCheckedChildren="正常"/>
            </Form.Item>
            <Form.Item name="avatar" label="头像" >
                <Input/>
            </Form.Item>
            <Form.Item name="wxOpenid" label="微信" >
                <Input/>
            </Form.Item>
            <Form.Item name="qqOpenid" label="QQ" >
                <Input/>
            </Form.Item>
            <Form.Item name="roleList" label="权限列表" >
                <Checkbox.Group options={roleList}  onChange={onChange} />
            </Form.Item>

        </Form>
    </Modal>
}

export default UserEdit;