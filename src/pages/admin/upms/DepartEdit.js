import {Modal, Input, Form, message, TreeSelect} from "antd";
import React, {useState} from "react";

const DepartEdit = (props) => {
    const {visible, confirmLoading, submitMap, onCancel, currentDetail, treeData} = props

    const formRef = React.createRef();
    const handleOk = () => {
        formRef.current.validateFields().then(values=>{
            submitMap(values)
        }).catch(err=>{
            message.error("部门信息保存失败")
        })
    }

    const [selectValue, setSelectValue] = useState(-1)

    const onChange = value => {
        setSelectValue(value)
    };

    return <Modal title="编辑服务" visible={visible}
                  onOk={handleOk}
                  confirmLoading={confirmLoading}
                  onCancel={onCancel}
                  destroyOnClose={true}
                  maskClosable={false}
                  width="50%">
        <Form ref={formRef} initialValues={currentDetail}>
            <Form.Item hidden={true} name="deptId">
                <Input/>
            </Form.Item>
            <Form.Item name="parentId" label="上级部门">
                <TreeSelect
                    style={{width: '100%'}}
                    value={selectValue}
                    dropdownStyle={{maxHeight: 400, overflow: 'auto'}}
                    treeData={treeData}
                    placeholder="请选择上级部门"
                    treeDefaultExpandAll
                    onChange={onChange} />
            </Form.Item>
            <Form.Item name="name" label="部门名称" rules={[{require: true, message: "请输入部门名称"}]}>
                <Input/>
            </Form.Item>

        </Form>
    </Modal>
}

export default DepartEdit;