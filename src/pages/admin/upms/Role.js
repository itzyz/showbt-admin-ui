import React, {useEffect, useState} from 'react'
import {Card, Button, Table, Popconfirm, message} from "antd";
import {getRoleList} from "@/services/upms/role";
import RoleEdit from "./RoleEdit";
import {getMenuTree} from "../../../services/upms/menu";
import {createTreeData} from "../../../utils/tools";
import {deleteRole} from "../../../services/upms/role";

function Role() {
    const [dataSource, setDataSource] = useState(null)
    const [roleId, setRoleId] = useState(null)
    const [menuTreeData, setMenuTreeData] = useState(null)

    //用于刷新列表
    const refreshRoleList = () => {
        getRoleList().then(res => {
            setDataSource(res.data.data)
        }).catch(err => {
            message.error("权限列表获取失败")
        })
    }

    useEffect(() => {
        getMenuTree().then(res => {
            let result = []
            createTreeData(result, res.data.data)
            setMenuTreeData(result)
        })
        refreshRoleList();
    }, [])

    const showModalAdd = () => {
        setRoleId(-1)
    }

    const showModalUpdate = (id) => {
        setRoleId(id)
    }

    const deleteRoleById = (id) => {
        deleteRole(id).then(res => {
            refreshRoleList()
        }).catch(err => {
            message.error("删除角色失败～")
        })
    }

    const columns = [
        {
            title: "角色ID",
            key: "roleId",
            dataIndex: "roleId"
        },
        {
            title: "角色名称",
            key: "roleName",
            dataIndex: "roleName"
        },
        {
            title: "角色标识",
            key: "roleCode",
            dataIndex: "roleCode"
        },
        {
            title: "角色描述",
            key: "roleDesc",
            dataIndex: "roleDesc"
        },
        {
            title: "创建时间",
            key: "createTime",
            dataIndex: "createTime"
        },
        {
            title: "操作",
            width: "15%",
            render: (txt, record, index) => {
                return <div>
                    <Button type="primary" size="small"
                            onClick={() => showModalUpdate(record.roleId)}>修改和授权</Button>
                    <Popconfirm title="确定要删除这个角色吗？"
                                onCancel={() => {
                                    message.info("取消删除")
                                }}
                                onConfirm={() => {
                                    deleteRoleById(record.roleId)
                                }}
                    >
                        <Button type="danger" size="small" style={{marginLeft: "1rem"}}>删除</Button>
                    </Popconfirm>
                </div>
            }
        }
    ]

    return <>
        <Card title="角色管理" extra={<Button type="primary" onClick={() => showModalAdd()}>新增</Button>}>
            <Table rowKey={columns => columns.roleId} columns={columns} dataSource={dataSource}/>
        </Card>
        {
            roleId ? <RoleEdit
                visible={true}
                roleId={roleId}
                menuTreeData={menuTreeData}
                onCancel={() => {
                    setRoleId(null)
                }}
                submitMap={() => {
                    message.success("角色信息保存成功")
                    setRoleId(null)
                    refreshRoleList();
                }}
            ></RoleEdit> : null
        }
    </>
}

export default Role