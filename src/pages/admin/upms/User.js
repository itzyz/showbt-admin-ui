import React, {useEffect, useState} from 'react'
import {Button, Card, message, Popconfirm, Table, Tag} from "antd";
import UserEdit from "./UserEdit";
import getUserPage, {deleteUser, getUserById, saveOrUpdateUser} from "../../../services/upms/user";
import {getDepartTree} from "../../../services/upms/depart";
import {createTreeData} from "../../../utils/tools";
import {getRoleList} from "../../../services/upms/role";

function User() {
    const [treeData, setTreeData] = useState([])
    const [roleList, setRoleList] = useState([])
    const [dataSource, setDataSource] = useState([])
    const [visible, setVisible] = useState(false)
    const [confirmLoading, setConfirmLoading] = useState(false)
    const [currentDetail, setCurrentDetail] = useState({})

    useEffect(() => {
        refreshTreeData();
        //获取部门树
        getDepartTree().then(res => {
            let data = []
            createTreeData(data, res.data.data)
            setTreeData(data)
        }).catch(err => {
            console.log(err)
            message.error("获取部门列表失败")
        })
        //获取权限列表
        getRoleList().then(res => {
            let plainOptions = []
            res.data.data.forEach(role => {
                plainOptions.push({label: role.roleName, value: role.roleId})
            })
            setRoleList(plainOptions)
        }).catch(err => {
            console.log(err)
            message.error("获取权限列表失败")
        })
    }, [])


    const refreshTreeData = () => {
        getUserPage().then(res => {
            setDataSource(res.data.data.records)
        }).catch(err => {
            message.error("获取用户列表失败！！！")
        })
    }

    const save = (values) => {
        values.lockFlag = values.lockFlag ? "1" : "0"
        values.role = values.roleList
        if (values.password === "") {
            values.password = null
        }
        saveOrUpdateUser(values).then(res => {
            if (res.status === 200) {
                refreshTreeData()
                setVisible(false)
            } else {
                message.error("用户信息保存失败！")
            }
        }).catch(err => {
            message.error("用户信息保存失败！")
        })
    }

    //点击新增按钮时触发此事件
    const showModalAdd = () => {
        setVisible(true)
        setCurrentDetail({})
    }

    //点击更新按钮时触发此事件
    const showModalUpdate = (id) => {
        getUserById(id).then(res => {
            const user = res.data.data

            const defaultList = []
            user.roleList.forEach(role => {
                defaultList.push(role.roleId)
            })
            user.roleList = defaultList
            user.lockFlag = parseInt(user.lockFlag)
            user.deptId = user.deptId.toString()
            user.password = ""
            setVisible(true)
            setCurrentDetail(user)
        }).catch(err => {
            message.error("获取用户信息失败")
        })
    }

    const columns = [
        {
            title: '用户ID ',
            dataIndex: 'userId',
            key: 'userId',
            width: '5%',
        },
        {
            title: '用户名称',
            dataIndex: 'username',
            key: 'username',
        },
        {
            title: '所属部门',
            dataIndex: 'deptName',
            key: 'deptName',
        },

        {
            title: '创建时间',
            dataIndex: 'createTime',
            key: 'createTime',
        },
        {
            title: '手机号码',
            dataIndex: 'phone',
            key: 'phone',
        },
        {
            title: '是否锁定',
            dataIndex: 'lockFlag',
            key: 'lockFlag',
            render: (txt, record, index) => txt === "0" ? <Tag color="#87d068">正常</Tag> : <Tag color="#f50">已锁定</Tag>
        },
        {
            title: '是否删除',
            dataIndex: 'delFlag',
            key: 'delFlag',
            render: (txt, record, index) => txt === "0" ? <Tag color="#87d068">正常</Tag> : <Tag color="#f50">已删除</Tag>
        },
        {
            title: "操作",
            render: (txt, record, index) => {
                if (record.delFlag === "0") {
                    return <>
                        <Button type="primary" size="small"
                                onClick={() => showModalUpdate(record.userId)}>修改</Button>
                        <Popconfirm title="确定删除用户吗？"
                                    onCancel={() => {
                                        message.info("删除操作已取消！")
                                    }}
                                    onConfirm={() => {
                                        console.log(record)
                                        deleteUser(record.userId).then(res => {
                                            if (res.status === 200) {
                                                message.info("用户已经删除！")
                                                refreshTreeData();
                                            } else {
                                                message.error("用户删除失败");
                                            }
                                        }).catch(err => {
                                            console.log(err)
                                            message.error("用户删除失败");
                                        })
                                    }}>
                            <Button type="danger" size="small" style={{marginLeft: "1rem"}}>删除</Button>
                        </Popconfirm>
                    </>
                }
            }
        }
    ];

    return (
        <Card title="用户列表" extra={<Button type="primary" onClick={showModalAdd}>新增</Button>}>
            <Table
                columns={columns}
                // rowSelection={{ ...rowSelection, checkStrictly }}
                dataSource={dataSource}
                rowKey={columns => columns.userId}
            />
            <UserEdit
                visible={visible}
                confirmLoading={confirmLoading}
                currentDetail={currentDetail}
                onCancel={() => {
                    setVisible(false)
                }} currentDetailData={{}}
                submitMap={(values) => {
                    save(values)
                }}
                treeData={treeData}
                roleList={roleList}
            ></UserEdit>
        </Card>
    );
}

export default User