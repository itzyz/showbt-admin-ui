import React from 'react'
import {Table, Card, Button, Popconfirm, message} from "antd";
import {getDepartById, getDepartTree, saveOrUpdateDepart} from "../../../services/upms/depart";
import DepartEdit from "./DepartEdit";
import {createDepartTreeData} from "../../../utils/tools";

class Depart extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            dataSource: [{
                id: "1",
                name: "新华网",
                createTime: "xxx"
            }],
            visible: false,
            confirmLoading: false,
            currentDetail: {}
        }
    }

    componentDidMount() {
        this.refreshDepartList()
    }

    refreshDepartList = ()=>{
        getDepartTree().then(res => {
            console.log(res.data)
            let treeResult = []
            createDepartTreeData(treeResult, res.data.data)
            this.setState({
                dataSource: res.data.data,
                treeData:[{title:"顶级部门", key:0, value: 0, children:treeResult}]
            })
        })
    }


    submitMap = values => {
        console.log(values)
        saveOrUpdateDepart(values).then(res=>{
            this.refreshDepartList()
            this.setState({
                visible:false
            })
        })

    }

    showModalUpdate = (id) => {
        console.log(id)
        getDepartById(id).then(res=>{
            console.log(res.data)
            this.setState({
                currentDetail:res.data.data,
                visible: true,
            })
        }).catch(err=>{
            message.error("获取部门信息失败！")
        })

    }

    columns = [
        {
            title: "部门名称",
            key: "name",
            dataIndex: "name",
        },
        {
            title: "创建时间",
            key: "createTime",
            dataIndex: "createTime",
        },
        {
            title: "操作",
            width: "12%",
            render: (txt, record, index) => {
                return <>
                    <Button type="primary" size="small" onClick={() => {
                        this.showModalUpdate(record.id)
                    }}>修改</Button>
                    <Popconfirm title="确认删除部门吗？" onConfirm={() => {
                        console.log("已删除")
                    }} onCancel={() => {
                        console.log("取消删除！")
                    }}>
                        <Button type="danger" style={{marginLeft: "1em"}} size="small">删除</Button>
                    </Popconfirm>
                </>
            }
        }
    ]

    render() {
        const {dataSource, visible, confirmLoading, currentDetail, treeData} = this.state;
        return (
            <Card title="部门列表" extra={<Button type="primary" onClick={() => {
                this.setState({visible: true, currentDetail: {}})
            }}>新增</Button>}>
                <Table rowKey={column => column.id} columns={this.columns}
                       pagination={false}
                       bordered
                       dataSource={dataSource}/>
                <DepartEdit visible={visible}
                            confirmLoading={confirmLoading}
                            submitMap={this.submitMap}
                            currentDetail={currentDetail}
                            onCancel={() => this.setState({visible: false, currentDetail: {}})}
                            treeData={treeData}
                >

                </DepartEdit>
            </Card>
        )
    }
}

export default Depart
