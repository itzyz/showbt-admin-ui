import {Modal, Input, Form, message, Tree} from "antd";
import React, {useEffect, useState} from "react";
import {getMenuTreeByRoleId, getRoleById, saveMenuTreeByRoleId, updateOrSaveRole} from "@/services/upms/role";

const formRef = React.createRef();

function RoleEdit(props) {
    const {roleId, onCancel, submitMap,menuTreeData} = props
    const [listRoleMenuData, setListRoleMenuData] = useState(null)
    const [currentDetail, setCurrentDetail] = useState(null)

    useEffect(() => {
        if (roleId && roleId !== -1) {
            getRoleById(roleId).then(res => {
                getMenuTreeByRoleId(roleId).then(res1 => {
                    let rmd = []
                    res1.data.map(r => {
                        return rmd.push(r + "")
                    })
                    setCurrentDetail(res.data.data)
                    setListRoleMenuData(rmd)
                }).catch(e1 => {
                    message.destroy()
                    message.error("获取角色菜单失败！")
                })
            }).catch(err => {
                message.destroy()
                message.error("角色信息获取失败！")
            })
        } else {
            setCurrentDetail({})
        }
    }, [roleId])

    const handleOk = () => {
        formRef.current.validateFields().then(values => {
            updateOrSaveRole(values).then(res => {
                if (values.roleId) {
                    saveMenuTreeByRoleId(values.roleId, listRoleMenuData).then(res => {
                        submitMap()
                    }).catch(err => {
                        message.destroy()
                        message.error("角色菜单信息保存失败！")
                    })
                }else {
                    submitMap()
                }
            }).catch(err => {
                message.destroy()
                message.error("角色信息保存接口异常！")
            })
        }).catch(err => {
            message.destroy()
            message.error("角色信息保存失败！")
        })
    }

    let hiddenMenuTree = currentDetail && currentDetail.roleId ? false : true

    const doCheckedEvent = (checkedKeys, {checked, checkedNodes, node, event, halfCheckedKeys}) => {
        if (checked) {
            let dataList = goDropLoop(menuTreeData, node.value, [])
            let temp = [...listRoleMenuData]
            for (let i = 0; i < dataList.length; i++) {
                const item = dataList[i]
                if (temp.indexOf(item) === -1) {
                    temp.push(item)
                }
            }
            let childList = getChildLoop(menuTreeData, node.value, [])
            if (childList) {
                for (let i = 0; i < childList.length; i++) {
                    let item = childList[i]
                    if (temp.indexOf(item) === -1) {
                        temp.push(item)
                    }
                }
            }
            setListRoleMenuData(temp)
        } else {
            goDropLoop(menuTreeData, node.value, [])
            let temp = [...listRoleMenuData]
            let childList = getChildLoop(menuTreeData, node.value, [])
            if (childList) {
                for (let i = 0; i < childList.length; i++) {
                    let item = childList[i]
                    if (temp.indexOf(item) !== -1) {
                        temp.splice(temp.indexOf(item), 1)
                    }
                }
            }
            setListRoleMenuData(temp)
        }
    }

    const getChildLoop = (loopData, searchKey, list, isChild = false) => {
        for (let i = 0; i < loopData.length; i++) {
            const element = loopData[i]
            if (isChild) {
                list.push(element.value)
            }
            if (element.value === searchKey) {
                list.push(element.value)
                if (element.children) {
                    getChildLoop(element.children, searchKey, list, true)
                }
                break
            } else if (element.children) {
                getChildLoop(element.children, searchKey, list, isChild)
            }
        }
        return list
    }

    const goDropLoop = (loopData, searchKey, list) => {
        for (let i = 0; i < loopData.length; i++) {
            const element = loopData[i]
            if (element.value === searchKey) {
                list.push(element.value)
                return list
            } else if (element.children) {
                list.push(element.value)
                let item = goDropLoop(element.children, searchKey, list)
                if (item) {
                    return item
                }
                list.length = list.length - 1
            }
        }
        return null
    }
    return <Modal title="角色管理" visible={true}
                  onOk={handleOk}
                  onCancel={onCancel}
                  destroyOnClose={true}
                  maskClosable={false}
                  width="50%">
        {currentDetail ? <Form ref={formRef} initialValues={currentDetail}>
            <Form.Item hidden={true} name="roleId">
                <Input/>
            </Form.Item>
            <Form.Item name="roleName" label="角色名称" rules={[{require: true, message: "请输入角色名称"}]}>
                <Input/>
            </Form.Item>
            <Form.Item name="roleCode" label="角色标识" rules={[{require: true, message: "请输入角色标识"}]}>
                <Input/>
            </Form.Item>
            <Form.Item name="roleDesc" label="角色描述">
                <Input.TextArea/>
            </Form.Item>
            <Form.Item name="permission" label="权限" hidden={hiddenMenuTree}>
                <Tree
                    checkable
                    defaultExpandAll={true}
                    onCheck={doCheckedEvent}
                    checkedKeys={listRoleMenuData}
                    treeData={menuTreeData}
                    showLine={true}
                    checkStrictly={true}
                />
            </Form.Item>
        </Form> : null}
    </Modal>
}

export default RoleEdit;