import {Tree, Form, Modal, Input, message, TreeSelect, Switch} from "antd";
import React, {useEffect, useState} from "react";
import {getAppById, getServerByAppId, saveOrUpdateApp} from "../../../services/gateway/app";

export default function AppEdit({appId, appTreeData, serverTreeData, onCancel, onOkSubmit}) {

    const [currentDetail, setCurrentDetail] = useState(null);
    const [selectedServers, setSelectedServers] = useState([]);
    const [expandedKeys, setExpandedKeys] = useState([]);

    const [selectValue, setSelectValue] = useState(-1)

    const [autoExpandParent, setAutoExpandParent] = useState(true)
    const [form] = Form.useForm();

    useEffect(() => {
        if (appId && appId > 0) {
            getAppDetailByAppId();
        } else {
            setCurrentDetail({})
        }
    }, [appId])

    const getAppDetailByAppId = () => {
        getAppById(appId).then(res => {
            setCurrentDetail(res.data.data)
        }).catch(err => {
            message.destroy();
            message.error("获取应用信息失败")
        })
        getServerByAppId(appId).then(res=>{
            const servers = res.data.data;
            let selectedServers = []
            servers.forEach(s => {
                selectedServers.push(s.id)
            })
            setSelectedServers(selectedServers)
            setExpandedKeys(selectedServers)
        }).catch(err=>{
            message.destroy();
            message.error("获取应用信息失败")
        })
    }

    const save = values => {
        let servers = []
        selectedServers.forEach(s => {
            servers.push({"id": s})
        })
        values.serversForm = servers;
        saveOrUpdateApp(values).then(res => {
            onOkSubmit()
        }).catch(err => {
            message.destroy();
            message.error("应用信息保存失败")
        })
    }

    const onExpand = expandedKeys => {
        setAutoExpandParent(false)
        setExpandedKeys(expandedKeys)
    };

    const onCheck = checkedKeys => {
        setSelectedServers(checkedKeys)
    };

    const onSelect = (selectedKeys, info) => {
        setSelectedServers(selectedKeys)
    };

    const handleOk = () => {
        form.validateFields().then(value => {
            value.status = value.status ? 1 : 0
            save(value)
        }).catch(err => {
            message.destroy()
            message.error("应用信息提交失败")
        })
    }

    const onChange = value => {
        setSelectValue(value)
    };

    return <Modal title="编辑应用" visible={true}
                  onOk={handleOk}
                  onCancel={onCancel}
                  destroyOnClose={true}
                  maskClosable={false}
                  width="60%"
    >
        {currentDetail ? <Form form={form} initialValues={currentDetail}>
            <Form.Item name="id" hidden={true}>
                <Input/>
            </Form.Item>
            <Form.Item name="parentId" label="上级应用">
                <TreeSelect
                    style={{width: '100%'}}
                    value={selectValue}
                    dropdownStyle={{maxHeight: 400, overflow: 'auto'}}
                    treeData={appTreeData}
                    placeholder="请选择上级应用"
                    treeDefaultExpandAll
                    onChange={onChange}/>
            </Form.Item>
            <Form.Item name="appName" label="应用名称" rules={[{required: true, message: "请输入应用名称"}]}>
                <Input/>
            </Form.Item>
            <Form.Item name="status" label="应用状态" valuePropName="checked">
                <Switch/>
            </Form.Item>
            <Form.Item name="servers" label="应用权限">
                <Tree
                    checkable
                    onExpand={onExpand}
                    expandedKeys={expandedKeys}
                    autoExpandParent={autoExpandParent}
                    onCheck={onCheck}
                    checkedKeys={selectedServers}
                    onSelect={onSelect}
                    selectedKeys={selectedServers}
                    treeData={serverTreeData}
                />
            </Form.Item>
        </Form> : null
        }
    </Modal>
}