import {Table, Card, message, Tag, Button, Popconfirm} from "antd"
import {connect} from "react-redux"

import React, {useState, useEffect} from "react";
import ServerEditModal from "./ServerEdit";
import {deleteServer, getServerList, saveOrUpdateServer} from "@/services/gateway/server";
import {getAllRules} from "@/services/gateway/rules";
import {CONFIG} from "@/utils/config";
import {setServerId} from "@/store/actions/serverAction";

const pageSize = CONFIG.pageSize

const ServerList = (props)=> {
    const projectId = props.match.params.pid
    const moduleId = props.match.params.mid;
    if (props.location.state) {
        sessionStorage.setItem(CONFIG.KEY_MODULE_LABEL_NAME, props.location.state.moduleName)
    }
    let projectName = sessionStorage.getItem(CONFIG.KEY_PROJECT_LABEL_NAME);
    let moduleName = sessionStorage.getItem(CONFIG.KEY_MODULE_LABEL_NAME);

    // eslint-disable-next-line no-unused-vars
    const [confirmLoading, setConfirmLoading] = useState(false)
    const [filterOptions, setFilterOptions] = useState()
    const [predicateOptions, setPredicateOptions] = useState()

    const [selectedId, setSelectedId] = useState(null)
    const [dataSource, setDataSource] = useState(null)
    const [totalElements, setTotalElements] = useState(null)
    const [currentPage, setCurrentPage] = useState(1)

    useEffect(() => {

        getServerListByPage(1, pageSize)
        initRules()
    }, [])// eslint-disable-line react-hooks/exhaustive-deps

    //点击新增按钮时触发此事件
    const showModalAdd = () => {
        //清空表单默认值
        setSelectedId(0)
    }

    //点击更新按钮时触发此事件
    const showModalUpdate = (id) => {
        setSelectedId(id)
    }

    const getServerListByPage = (page, pageSize) =>{
        getServerList(moduleId, page, pageSize).then(res => {
            if (res.data.code === 200) {
                setDataSource(res.data.data.content)
                setTotalElements(res.data.data.totalElements)
                setCurrentPage(page)
            }
        }).catch(err => {
            message.destroy()
            message.error("获取服务列表失败")
        })
    }

    //获取所有过滤器和匹配规则，配置服务时待选
    const initRules = () => {
        getAllRules().then(res => {
            if (res.data.code === 200) {
                let ar = res.data.data.content
                let filters = []
                let predicates = []
                for (let i in ar) {
                    if (ar[i].ruleType === "filter") {
                        filters.push({value: ar[i].id, label: ar[i].ruleName, args: ar[i].args})
                    } else {
                        predicates.push({value: ar[i].id, label: ar[i].ruleName, args: ar[i].args})
                    }
                }
                setFilterOptions(filters)
                setPredicateOptions(predicates)
            }
        }).catch(err => {
            message.destroy()
            message.error("获取过滤器失败")
        })
    }

    /**
     * 点击modal确认按钮后，提交表单数据，并刷新列表页
     * @param values
     */
    const saveServer = (values) => {
        saveOrUpdateServer({...values, module: {id: moduleId}}).then(res => {
            setSelectedId(null)
            getServerListByPage(currentPage, pageSize)
        }).catch(err => {
            message.destroy()
            message.error("数据保存失败！！")
        })
    }

    const delServer = id => {
        deleteServer(id).then(res => {
            message.success("服务删除成功！");
            getServerListByPage(currentPage, pageSize)
        }).catch(err => {
            message.destroy()
            message.error("服务删除失败！");
        })
    }

    const columns = [
        {
            title: "序号",
            key: "id",
            width: 80,
            align: "center",
            render: (txt, record, index) => index + 1
        },
        {
            title: "服务名称",
            dataIndex: "serverName"
        },
        {
            title: "虚拟路径",
            dataIndex: "label"
        },
        {
            title: "真实URL",
            dataIndex: "realPath"
        },
        {
            title: "请求方式",
            dataIndex: "method"
        },
        {
            title: "创建时间",
            dataIndex: "createTime"
        },
        {
            title: "状态",
            dataIndex: "status",
            render: (txt, record, index) => txt === 0 ? <Tag color="#f50">无效</Tag> : <Tag color="#87d068">有效</Tag>
        },
        {
            title: "网关地址",
            render: (txt, record, index) => record.module.project.label + record.module.label + record.label
        },
        {
            title: "操作",
            render: (txt, record, index) => {
                return <div>
                    <Button type="primary" size="small" onClick={() => {
                        showModalUpdate(record.id)
                        props.setCurrentServerId(record.id)
                    }}>编辑</Button>
                    <Popconfirm title="确定要删除此服务吗？"
                                onCancel={() => {
                                    message.info("取消删除操作")
                                }} onConfirm={() => delServer(record.id)}
                    >
                        <Button type="danger" size="small" style={{margin: "0 1rem"}}>删除</Button>
                    </Popconfirm>
                </div>
            }
        }
    ]
    return <Card title={`服务列表【${projectName}>>${moduleName}】`}
                 extra={<>
                     <Button type="primary" onClick={showModalAdd}>新增</Button>
                     <Button type="default"
                             onClick={() => props.history.push({
                                 pathname: `/admin/module/list/${projectId}`,
                                 query: {
                                     projectId: projectId,
                                     projectName: projectName,
                                     moduleName: moduleName
                                 }
                             })}>返回</Button>
                 </>}>
        <Table rowKey={col => col.id}
               columns={columns}
               dataSource={dataSource}
               pagination={{
                   total: totalElements,
                   defaultPageSize: pageSize,
                   onChange: getServerListByPage
               }}
        />
        {
            selectedId !== null ? <ServerEditModal
                confirmLoading={confirmLoading}
                filterOptions={filterOptions}
                predicateOptions={predicateOptions}
                onCancel={() => {
                    setSelectedId(null)
                }}
                submitMap={(values) => {
                    saveServer(values)
                }}
                recordId={selectedId}
            >
            </ServerEditModal> : null
        }

    </Card>
}

const mapDispatchToProps = (dispatch) => ({
    setCurrentServerId(data) {
        const action = setServerId(data)
        dispatch(action)
    },
})
export default connect(null, mapDispatchToProps)(ServerList)