import {Table, Card, Button, Popconfirm, message, Tag} from "antd";
import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {setProjectInfo} from "@/store/actions/projectAction";
import ProjectEdit from "./ProjectEdit";
import {deleteProject, getProjectList, saveOrUpdateProject} from "@/services/gateway/project";
import {CONFIG} from "@/utils/config";

const pageSize = CONFIG.pageSize

function ProjectList(props) {
    const [dataSource, setDataSource] = useState(null)
    const [totalElements, setTotalElements] = useState(null)
    const [currentPage, setCurrentPage] = useState(1)

    useEffect(() => {
        getProjectListByPage(1, pageSize);
    }, [])

    const getProjectListByPage = (page, pageSize) => {
        getProjectList(page, pageSize).then(res => {
            if (res.data.code === 200) {
                setDataSource(res.data.data.content)
                setTotalElements(res.data.data.totalElements)
                setCurrentPage(page)
            }
        }).catch(err => {
            message.destroy()
            message.error("获取项目列表失败")
        })
    }

    //点击分页时加载数据
    const projectListByPage = (page, pageSize) => {
        getProjectListByPage(page, pageSize)
    }

    const [projectId, setProjectId] = useState(null)
    // eslint-disable-next-line no-unused-vars
    const [confirmLoading, setConfirmLoading] = useState(false)


    //点击新增按钮时触发此事件
    const showModalAdd = () => {
        //清空表单默认值
        setProjectId(0)
    }

    //点击更新按钮时触发此事件
    const showModalUpdate = (id) => {
        setProjectId(id)
    }

    const saveProject = (values) => {
        if (values.status) {
            values.status = 1
        } else {
            values.status = 0
        }
        saveOrUpdateProject(values).then(res => {
            setProjectId(null)
            getProjectListByPage(currentPage, pageSize)
        }).catch(err => {
            message.warning("更新失败！！");
        })
    }

    const delProject = (id) => {
        deleteProject(id).then(res => {
            message.success("项目删除成功！")
            // dispatch(loadProject({page: project.page, pageSize: project.pageSize}))
            getProjectListByPage(currentPage, pageSize)
        }).catch(err => {
            message.error("项目删除失败！")
        })
    }

    const columns = [
        {
            title: "序号",
            key: "id",
            width: 80,
            align: "center",
            render: (txt, record, index) => record.id.substring(record.id.length - 10, record.id.length)
        },
        {
            title: "项目名称",
            dataIndex: "projectName"
        },
        {
            title: "创建时间",
            dataIndex: "createTime"
        },
        {
            title: "项目状态",
            dataIndex: "status",
            render: (txt, record, index) => txt === 0 ? <Tag color="#f50">无效</Tag> : <Tag color="#87d068">有效</Tag>
        },
        {
            title: "虚拟路径",
            dataIndex: "label"
        },
        {
            title: "操作",
            render: (txt, record, index) => {
                return <div>
                    <Button type="primary" size="small"
                            onClick={() => {
                                props.setCurrentProjectName({id: record.id, name: record.projectName})
                                props.history.push({
                                    pathname: `/admin/module/list/${record.id}`,
                                    query: {
                                        projectId: record.id,
                                        projectName: record.projectName
                                    },
                                })
                            }

                            }>模块</Button>
                    <Button type="primary" size="small"
                            style={{marginLeft: "1rem"}}
                            onClick={() => showModalUpdate(record.id)}>编辑</Button>
                    <Popconfirm title="警告：删除项目时自动删除项目下的模块和服务，确定要删除吗？"
                                onCancel={() => {
                                    message.info("取消删除操作")
                                }} onConfirm={() => delProject(record.id)
                    }
                    >
                        <Button type="danger" size="small" style={{margin: "0 1rem"}}>删除</Button>
                    </Popconfirm>
                </div>
            }
        }
    ]

    return <Card title="项目列表"
                 extra={<Button type="primary" onClick={showModalAdd}>新增</Button>}>
        <Table
            columns={columns}
            rowKey={columns => columns.id}
            dataSource={dataSource}
            pagination={{
                total: totalElements, defaultPageSize: 10, onChange: projectListByPage
            }}
        />
        {projectId != null ? <ProjectEdit
            confirmLoading={confirmLoading}
            projectId={projectId}
            onCancel={() => {
                setProjectId(null)
            }}
            submitMap={(values) => {
                saveProject(values)
            }}></ProjectEdit> : null}
    </Card>
}


const mapDispatchToProps = (dispatch) => {
    return {
        setCurrentProjectName(data) {
            const action = setProjectInfo(data)
            dispatch(action)
        }
    }
}

export default connect(null, mapDispatchToProps)(ProjectList);