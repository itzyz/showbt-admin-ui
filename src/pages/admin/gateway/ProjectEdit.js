import React, {useEffect, useState} from "react";
import {Form, Input, Modal, Switch, message} from "antd";
import {getProjectById} from "../../../services/gateway/project";

const ProjectEdit = (props) => {
    const {confirmLoading, submitMap, onCancel, projectId} = props
    // const formRef = React.createRef();
    const [form] = Form.useForm();
    const [currentProject, setCurrentProject] = useState(null)

    useEffect(() => {
        const getCurrentProject = () => {
            getProjectById(projectId).then(res => {
                setCurrentProject(res.data.data)
            }).catch(err => {
                message.destroy()
                message.error("获取项目详细信息失败！")
            })
        }
        if (projectId !== 0) {
            getCurrentProject()
        } else {
            setCurrentProject({})
        }
    }, [projectId])

    const handleOk = () => {
        form.validateFields().then(values => {
            submitMap(values)
        }).catch(err => {
            message.destroy()
            message.info("保存项目信息失败！！！")
        })
    }

    return <Modal title="编辑服务" visible={true}
                  onOk={handleOk}
                  confirmLoading={confirmLoading}
                  onCancel={onCancel}
                  destroyOnClose={true}
                  maskClosable={false}
                  width="50%">
        {currentProject ? <Form form={form} initialValues={currentProject}>
            <Form.Item hidden="true" name="id">
                <Input/>
            </Form.Item>
            <Form.Item label="项目名称" name="projectName" rules={[{required: true, message: "请输入项目名称！"}]}>
                <Input placeholder="请输入项目名称"/>
            </Form.Item>
            <Form.Item label="虚拟路径" name="label" rules={[{required: true, message: "请输入虚拟路径，并且不能与已有项目路径重复！"}]}>
                <Input placeholder="请输入项目虚拟路径,如：/abc"/>
            </Form.Item>
            <Form.Item label="项目状态" name="status" valuePropName="checked">
                <Switch/>
            </Form.Item>
            <Form.Item label="项目备注" name="remark">
                <Input.TextArea placeholder="请输入项目备注"/>
            </Form.Item>
        </Form> : null}
    </Modal>
}

export default ProjectEdit;