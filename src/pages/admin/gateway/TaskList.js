import React, {useEffect, useState} from 'react'
import {Table, Button, Card, Tag, Space, Popconfirm, message, Row, Col, Form, TreeSelect} from "antd";
import {deleteTaskById, getTaskList} from "@/services/gateway/task";
import {getAppList} from "@/services/gateway/app";
import {createAppTreeData} from "@/utils/tools";
import TaskEdit from "./TaskEdit";
import {CONFIG} from "../../../utils/config";

const pageSize = CONFIG.pageSize

function TaskList() {
    const [dataSource, setDataSource] = useState(null)
    const [appTreeData, setAppTreeData] = useState(null)
    const [taskId, setTaskId] = useState(null)
    const [searchAppId, setSearchAppId] = useState(null)
    const [totalElements, setTotalElements] = useState(null)
    const [currentPage, setCurrentPage] = useState(1)

    const [form] = Form.useForm();

    useEffect(() => {
        getTaskListByPage(1);
    }, [searchAppId])// eslint-disable-line react-hooks/exhaustive-deps
    useEffect(() => {
        refreshAppList();
    }, [])

    const getTaskListByPage = (page) => {
        getTaskList(searchAppId, page, pageSize).then(res => {
            setDataSource(res.data.data.content)
            setTotalElements(res.data.data.totalElements)
            setCurrentPage(page)
        }).catch(err => {
            message.destroy()
            message.error("获取任务列表失败")
        });
    }

    const refreshAppList = () => {
        getAppList().then(res => {
            let data = []
            createAppTreeData(data, res.data.data)
            setAppTreeData([{title: "顶级应用", key: 0, value: 0, children: data}])
        }).catch(err => {
            message.destroy()
            message.error("任务列表中获取应用列表失败")
        })
    }

    const onUpdateClick = (id) => {
        setTaskId(id)
    }

    const save = () => {
        getTaskListByPage(currentPage);
        setTaskId(null)
    }
    const onFinish = () => {

    }
    const searchTaskListByAppId = () => {
        form.validateFields().then(values => {
            setSearchAppId(values.searchAppId)
        })
    }

    const columns = [
        {
            key: "id",
            title: "任务ID",
            width: "180px",
            align: "center",
            render: (txt, record) => record.id
        },
        {
            title: "任务名称",
            dataIndex: "taskName"
        },
        {
            title: "应用名称",
            render: (txt, record) => record.app.appName
        },
        {
            title: "是否签名",
            // dataIndex:"isSign"
            render: (txt, record) => record.isSign === 1 ? <Tag color="#87d068">是</Tag> :
                <Tag color="#f50">否</Tag>
        },
        {
            title: "状态",
            render: (txt, record) => {
                if (record.status === 1) {
                    if (record.effect === 0) {
                        return <Tag color="#f50">未生效</Tag>
                    } else if (record.effect === 1) {
                        return <Tag color="#87d068">有效</Tag>
                    } else {
                        return <Tag color="#ccc">已过期</Tag>
                    }
                } else {
                    return <Tag color="#f50">无效</Tag>
                }
            }
        }, {
            title: "操作",
            render: (txt, record, index) => {
                return <Space>
                    <Button type="primary" size="small" onClick={() => {
                        onUpdateClick(record.id)
                    }}>修改</Button>
                    <Popconfirm title="确定要删除这个任务吗？"
                                onCancel={() => {
                                    message.info("取消删除操作！")
                                }}
                                onConfirm={() => {
                                    deleteTaskById(record.id).then(res => {
                                        getTaskListByPage();
                                    }).catch(err => {
                                        message.destroy();
                                        message.error("任务删除失败");
                                    });

                                }}
                    >
                        <Button type="danger" size="small">删除</Button>
                    </Popconfirm>
                </Space>
            }
        }
    ]
    return (
        <Card title="任务列表" extra={<Button type="primary" onClick={() => {
            setTaskId(0)
        }}>新增</Button>}>
            <Form
                form={form}
                name="advanced_search"
                className="ant-advanced-search-form"
                onFinish={onFinish}
                initialValues={{searchAppId:0}}
            >
                <Row gutter={24}>
                    <Col span={6} key={1}>
                        <Form.Item name="searchAppId" label="选择应用">
                            <TreeSelect
                                treeData={appTreeData}
                                treeDefaultExpandAll={true}
                                onChange={searchTaskListByAppId}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
            <Table rowKey={columns => columns.id}
                   columns={columns}
                   dataSource={dataSource}
                   pagination={{
                       total: totalElements,
                       defaultPageSize: pageSize,
                       onChange: getTaskListByPage
                   }}
            ></Table>
            {taskId != null ? <TaskEdit
                taskId={taskId}
                appTreeData={appTreeData}
                onCancel={() => {
                    setTaskId(null)
                }}
                onOkSubmit={save}
            >
            </TaskEdit> : null}
        </Card>
    )
}

export default TaskList