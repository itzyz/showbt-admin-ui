import {Input, Modal, Radio, Switch, Divider, Form, Select, Space, message} from "antd";
import {MinusCircleOutlined} from '@ant-design/icons';
import React, {useEffect, useState} from "react";

import {connect} from "react-redux"

import "./serverEdit.css"
import {getServerById} from "@/services/gateway/server";


const ServerEditModal = (props) => {
    const {confirmLoading, submitMap, onCancel, filterOptions, predicateOptions, recordId} = props

    // const formRef = React.createRef();
    const [form] = Form.useForm();

    const [filterSelected, setFilterSelected] = useState([])
    const [predicateSelected, setPredicateSelected] = useState([])

    const [currentDetail, setCurrentDetail] = useState(null)
    useEffect(() => {
        if (recordId !== 0) {
            getServerById(recordId).then(res => {
                let cd = res.data.data
                let filterSelected = []
                let predicateSelected = []
                for (let i in cd.configs) {
                    let cnf = cd.configs[i]
                    let v = {
                        fieldKey: cnf.pk.ruleDict.id,
                        isListField: true,
                        key: cnf.pk.ruleDict.id,
                        label: cnf.pk.ruleDict.ruleName,
                        args: cnf.args,
                        ruleId: cnf.pk.ruleDict.id,
                        name: cnf.pk.ruleDict.ruleName
                    }
                    if (cnf.pk.ruleDict.ruleType === "predicate") {
                        predicateSelected.push(v)
                    } else {
                        filterSelected.push(v)
                    }
                }
                cd.filterSelected = filterSelected
                cd.predicateSelected = predicateSelected
                //此处必须设置，否选择过滤器或规则时会清空默认已经中的
                setFilterSelected(filterSelected)
                setPredicateSelected(predicateSelected)
                delete cd.configs
                setCurrentDetail(cd)

            }).catch(err => {
                message.error("出错了。。vvvv。。。")
            })
        } else {
            setCurrentDetail({})
        }
    }, [recordId])

    const handleOk = () => {
        form.validateFields().then(values => {
            let filters = values.filterSelected
            let predicates = values.predicateSelected
            let configs = []
            covertConfig(configs, filters)
            covertConfig(configs, predicates)
            values.configs = configs
            delete values.filterSelected
            delete values.predicateSelected

            if (values.status) {
                values.status = 1
            } else {
                values.status = 0
            }
            if (values.requestHeaders) {
                values.requestHeaders = JSON.stringify(JSON.parse(values.requestHeaders))
            }
            if (values.fixedParams) {
                values.fixedParams = JSON.stringify(JSON.parse(values.fixedParams))
            }
            console.log("ok::::", values)
            submitMap(values)
        }).catch(err => {
            message.destroy()
            message.error("保存失败！")
        })
    }

    const covertConfig = (configs, filters) => {
        if (filters && filters.length > 0) {
            for (let i in filters) {
                let f = filters[i]
                let rs = {
                    "pk": {
                        "ruleDict": {
                            "id": f.ruleId
                        }
                    },
                    "args": f.args
                }
                configs.push(rs)
            }
        }
        return configs
    }

    //获取select中选中的内容
    const getValue = (value, option, selected) => {
        let v = {
            fieldKey: option.value,
            isListField: true,
            key: option.value,
            label: option.label,
            args: option.args,
            ruleId: option.value,
            name: option.label
        }
        let isAdd = true
        for (let x in selected) {
            let obj = selected[x]
            if (obj.key === v.key) {
                isAdd = false
                break
            }
        }
        if (isAdd)
            return v
        return null
    }

    //选择过滤器中的选项
    const filterHandleChange = (value, option) => {
        let v = getValue(value, option, filterSelected)
        if (v) {
            filterSelected.push(v)
        }
        // setFilterSelected(filterSelected)
        form.setFieldsValue({filterSelected});
    };

    //选择规则中的选项
    const predicateHandleChange = (value, option) => {
        let v = getValue(value, option, predicateSelected)
        if (v) {
            predicateSelected.push(v)
        }
        // setPredicateSelected(predicateSelected)
        form.setFieldsValue({predicateSelected});
    };

    return <Modal title="编辑服务" visible={true}
                  onOk={handleOk}
                  confirmLoading={confirmLoading}
                  onCancel={onCancel}
                  destroyOnClose={true}
                  maskClosable={false}
                  width="60%"
    >
        {currentDetail ? <Form form={form} className='serverEdit' initialValues={currentDetail} onFinish={handleOk}
                               autoComplete="off">
            <Form.Item hidden="true" name="id">
                <Input/>
            </Form.Item>
            <Form.Item name="serverName" label="服务名称" rules={[{required: true, message: "请输入服务名称！"}]}>
                <Input placeholder="请输入服务名称"/>
            </Form.Item>
            <Form.Item name="label" label="虚拟路径" rules={[{required: true, message: "请输入虚拟路径！"}]}>
                <Input placeholder="请输入虚拟路径"/>
            </Form.Item>
            <Form.Item name="realPath" label="真实路径" rules={[{required: true, message: "请输入真实路径！"}]}>
                <Input placeholder="请输入真实路径"/>
            </Form.Item>
            <Form.Item name="method" label="请求方式" rules={[{required: true, message: "请选择请求方式！"}]}>
                <Radio.Group placeholder="请选择请求方式">
                    <Radio value="GET">GET</Radio>
                    <Radio value="POST">POST</Radio>
                    <Radio value="PUT">PUT</Radio>
                    <Radio value="PATCH">PATCH</Radio>
                    <Radio value="DELETE">DELETE</Radio>
                </Radio.Group>
            </Form.Item>
            <Form.Item label="&nbsp;&nbsp;&nbsp;服务状态" name="status" valuePropName="checked">
                <Switch/>
            </Form.Item>
            <Divider>参数配置</Divider>
            <Form.Item name="requestHeaders" label="&nbsp;&nbsp;&nbsp;请求头部">
                <Input placeholder="请输入请求头部如：{&quot;Content-Type&quot;:&quot;application/json&quot;}"/>
            </Form.Item>
            <Form.Item name="fixedParams" label="&nbsp;&nbsp;&nbsp;固定参数">
                <Input placeholder="请输入真实路径如：{&quot;key&quot;:&quot;value&quot;}"/>
            </Form.Item>
            <Form.Item name="requestParamsKeys" label="&nbsp;&nbsp;&nbsp;请求参数">
                <Input.TextArea
                    placeholder="[{&quot;name&quot;:&quot;属性名称&quot;,&quot;type&quot;:&quot;属性值类型(int,string...)&quot;,&quot;desc&quot;:&quot;属性说明&quot;}]"/>
            </Form.Item>
            <Divider>过滤器与匹配规则配置</Divider>
            <Form.Item label="&nbsp;&nbsp;&nbsp;过&nbsp;&nbsp;滤&nbsp;器">
                <Input.Group compact>
                    <Form.Item name="filterOptions" noStyle={true}>
                        <Select
                            onChange={filterHandleChange}
                            options={filterOptions}
                            style={{width: '100%'}}
                        >
                        </Select>
                    </Form.Item>
                    <Form.List name="filterSelected">
                        {(fields, {add, remove}) => (
                            <>
                                {fields.map(({key, name, fieldKey, ...restField}) => (
                                    <Space key={key} style={{display: 'flex', width: '100%', marginTop: 10}}
                                           align="baseline">
                                        <Form.Item
                                            {...restField}
                                            name={[name, 'ruleId']}
                                            fieldKey={[fieldKey, 'ruleId']}
                                            rules={[{required: true, message: 'Missing ruleId '}]}
                                            style={{flex: '1'}}
                                        >
                                            <Input placeholder="Rule ID" readOnly={true}/>
                                        </Form.Item>
                                        <Form.Item
                                            {...restField}
                                            name={[name, 'label']}
                                            fieldKey={[fieldKey, 'label']}
                                            rules={[{required: true, message: 'Missing first name'}]}
                                            style={{flex: '1'}}
                                        >
                                            <Input placeholder="过滤器名称" readOnly={true}/>
                                        </Form.Item>
                                        <Form.Item
                                            {...restField}
                                            name={[name, 'args']}
                                            fieldKey={[fieldKey, 'args']}
                                            // rules={[{required: true, message: 'Missing args name'}]}
                                            style={{flex: '1'}}
                                        >
                                            <Input placeholder="过滤器参数"/>
                                        </Form.Item>
                                        <MinusCircleOutlined onClick={() => {
                                            remove(name)
                                            filterSelected.splice(name, 1)
                                        }}/>
                                    </Space>
                                ))}
                            </>
                        )}
                    </Form.List>
                </Input.Group>
            </Form.Item>

            <Form.Item label="&nbsp;&nbsp;&nbsp;匹配规则">
                <Input.Group compact>
                    <Form.Item name="predicateOptions" noStyle={true}>
                        <Select options={predicateOptions} onChange={predicateHandleChange} style={{width:'100%'}}>
                        </Select>
                    </Form.Item>
                    <Form.List name="predicateSelected">
                        {(fields, {add, remove}) => (
                            <>
                                {fields.map(({key, name, fieldKey, ...restField}) => (
                                    <Space key={key} style={{display: 'flex', width: '100%', marginTop: 10}}
                                           align="baseline">
                                        <Form.Item
                                            {...restField}
                                            name={[name, 'ruleId']}
                                            fieldKey={[fieldKey, 'ruleId']}
                                            rules={[{required: true, message: 'Missing ruleId '}]}
                                            style={{flex: '1'}}
                                        >
                                            <Input placeholder="Rule ID" readOnly={true}/>
                                        </Form.Item>
                                        <Form.Item
                                            {...restField}
                                            name={[name, 'label']}
                                            fieldKey={[fieldKey, 'label']}
                                            rules={[{required: true, message: 'Missing first name'}]}
                                            style={{flex: '1'}}
                                        >
                                            <Input placeholder="规则名称" readOnly={true}/>
                                        </Form.Item>
                                        <Form.Item
                                            {...restField}
                                            name={[name, 'args']}
                                            fieldKey={[fieldKey, 'args']}
                                            // rules={[{required: true, message: 'Missing args name'}]}
                                            style={{flex: '1'}}
                                        >
                                            <Input placeholder="规则参数"/>
                                        </Form.Item>
                                        <MinusCircleOutlined onClick={() => {
                                            remove(name)
                                            predicateSelected.splice(name, 1)
                                        }}/>
                                    </Space>
                                ))}
                            </>
                        )}
                    </Form.List>
                </Input.Group>
            </Form.Item>
            <Divider>其它配置</Divider>
            <Form.Item name="remark" label="&nbsp;&nbsp;&nbsp;备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注">
                <Input.TextArea placeholder="请输入备注"/>
            </Form.Item>
        </Form> : null}

    </Modal>
}

const mapStateToProps = (state) => {
    return {
        serverId: state.servers.get("serverId"),
    }
}

export default connect(mapStateToProps, null)(ServerEditModal);