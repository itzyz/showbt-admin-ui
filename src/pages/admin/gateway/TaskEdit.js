import React, {useEffect, useState} from "react"
import {Form, Button, Input, Modal, message, Switch, TreeSelect, DatePicker} from "antd"
import {PlusOutlined} from "@ant-design/icons"
import TextArea from "antd/lib/input/TextArea"
import moment from "moment-timezone";
import {getTaskById, saveOrUpdateTask} from "@/services/gateway/task";
import {getStrategyList} from "@/services/gateway/strategy";
import StrategyEdit from "./StrategyEdit";
import {connect} from "react-redux";
import {setTaskSelectedAppId} from "@/store/actions/taskAction";

const dateFormat = 'YYYY-MM-DD HH:MM:SS';

function TaskEdit(props) {
    const {onOkSubmit, onCancel, appTreeData, taskId} = props
    const [selectedAppId, setSelectedAppId] = useState(0)
    const [currentTask, setCurrentTask] = useState(null)
    const [strategyOptionData, setStrategyOptionData] = useState([])
    const [appId, setAppId] = useState(null)

    const [form] = Form.useForm()
    useEffect(() => {
        if (taskId !== 0) {
            getTaskById(taskId).then(res => {
                const obj = res.data;
                setSelectedAppId(obj.data.app.id)
                getStrategyListByAppId(obj.data.app.id,2)
                let strategyArr = []
                for (let i in obj.data.strategies) {
                    let strategy = obj.data.strategies[i]
                    strategyArr.push(strategy.id)
                }
                setCurrentTask({
                    ...obj.data,
                    appId: obj.data.app.id,
                    effectiveTime: moment(obj.data.effectiveTime || dateFormat),
                    expiryTime: moment(obj.data.expiryTime || dateFormat),
                    strategies: strategyArr
                })
            }).catch(err => {
                message.destroy();
                message.error("获取任务信息失败");
            })
        } else {
            setCurrentTask({})
        }
    }, [taskId])

    //根据应用ID获取应用所拥有的策略
    const getStrategyListByAppId = (appId,status) => {
        console.log("getStrategyListByAppId>>>", appId, "    status>>>", status)
        if (appId) {
            getStrategyList(appId,status, 1, 100).then(res => {
                console.log(res.data.data)
                if (res.data.code === 200) {
                    const sList = res.data.data.content
                    let options = []
                    for (let i in sList) {
                        let obj = sList[i]
                        options.push({
                            title: obj.strategyName,
                            value: obj.id,
                            key: obj.id,
                        })
                    }
                    setStrategyOptionData(options)
                }
            }).catch(err => {
                message.destroy()
                message.error("获取策略列表失败！！！")
            })
        } else {
            setStrategyOptionData([])
        }
    }

    //提交任务，保存任务信息
    const handleOk = () => {
        form.validateFields().then(value => {
            console.log("TaskEdit>>>>handleOk:::", value)
            value.status = value.status ? 1 : 0
            const effectiveTime = moment(new Date(value.effectiveTime)).tz("Asia/Shanghai").format(dateFormat);
            const expiryTime = moment(new Date(value.expiryTime)).tz("Asia/Shanghai").format(dateFormat);
            const isSign = value.isSign ? 1 : 0;
            const status = value.status ? 1 : 0;
            value.effectiveTime = effectiveTime;
            value.expiryTime = expiryTime;
            value.app = {id: value.appId};
            value.status = status;
            value.isSign = isSign
            if (value.strategies) {
                let sResArr = []
                for (let i in value.strategies) {
                    sResArr.push({id: value.strategies[i]})
                }
                value.strategies = sResArr
            }
            console.log("TaskEdit>>>>handleOk:::", value)
            saveOrUpdateTask(value).then(res => {
                if (res.data.code === 200) {
                    onOkSubmit(value)
                    message.destroy();
                    message.success("任务保存成功！");
                } else {
                    message.destroy();
                    message.error("任务接口异常!");
                }
            }).catch(err => {
                message.destroy();
                message.error("任务保存失败!");
            })

        }).catch(err => {
            message.destroy()
            message.error("任务信息提交失败")
        })
    }

    //选择应用
    const onChangeApp = value => {
        form.setFieldsValue({strategies:[]})
        setSelectedAppId(value)
        getStrategyListByAppId(value, 2)
    };

    //保存新增策略后，重新获取策略列表
    const strategyHandleOk = () => {
        getStrategyListByAppId(appId,2)
        setAppId(null)
    }

    //取消新增策略
    const strategyOnCancel = () => {
        setAppId(null)
    }

    //选择策略
    const onChangeStrategy = value => {
    };

    return <Modal title="编辑任务" visible={true}
                  onOk={handleOk}
                  onCancel={onCancel}
                  destroyOnClose={true}
                  maskClosable={false}
                  width="60%"
    >

        {currentTask ? <Form form={form} initialValues={currentTask}>
            <Form.Item name="id" hidden={true}>
                <Input/>
            </Form.Item>
            <Form.Item name="appId" label="所属应用" rules={[{required: true, message: "请选择应用"}]}>
                <TreeSelect
                    style={{width: '100%'}}
                    value={selectedAppId}
                    dropdownStyle={{maxHeight: 400, overflow: 'auto'}}
                    treeData={appTreeData}
                    placeholder="请选择上级应用"
                    treeDefaultExpandAll
                    onChange={onChangeApp}/>
            </Form.Item>
            <Form.Item name="taskName" label="任务名称" rules={[{required: true, message: "请输入任务名称"}]}>
                <Input/>
            </Form.Item>
            <Form.Item label="策略列表">
                <Input.Group compact>
                    <Form.Item name="strategies" noStyle={true} rules={[{required: true, message: '请选择策略'}]}>
                        <TreeSelect style={{width: '80%'}}
                                    treeData={strategyOptionData}
                                    treeCheckable={true}
                                    onChange={onChangeStrategy}
                                    placeholder="请选择策略"
                        />
                    </Form.Item>

                    <Button type="primary" shape="circle" style={{display: "inline-block"}} icon={<PlusOutlined/>}
                            onClick={() => {
                                console.log("currentTask:::", currentTask)
                                props.setSelectedAppId(selectedAppId)
                                setAppId(selectedAppId)
                            }}
                    />
                </Input.Group>
            </Form.Item>

            {/*</Form.Item>*/}
            <Form.Item style={{marginBottom: 0}} label="&nbsp;&nbsp;&nbsp;生效时间">
                <Form.Item name="effectiveTime" style={{display: 'inline-block', marginRight: 10}}>
                    <DatePicker showTime/>
                </Form.Item>
                <Form.Item name="expiryTime" style={{display: 'inline-block'}}>
                    <DatePicker showTime/>
                </Form.Item>
            </Form.Item>
            <Form.Item name="isSign" label="&nbsp;&nbsp;&nbsp;是否签名" valuePropName="checked">
                <Switch/>
            </Form.Item>
            <Form.Item name="status" label="&nbsp;&nbsp;&nbsp;任务状态" valuePropName="checked">
                <Switch/>
            </Form.Item>
            <Form.Item name="remark" label="&nbsp;&nbsp;&nbsp;任务备注">
                <TextArea/>
            </Form.Item>
        </Form> : null}


        {appId ?
            <StrategyEdit
                onOk={strategyHandleOk}
                onCancel={strategyOnCancel}
                appTreeData={appTreeData}
                selectedAppId={selectedAppId}
                destroyOnClose={true}
                maskClosable={false}
            >
            </StrategyEdit> : null
        }
    </Modal>
}

const mapDispatchToProps = (dispatch) => {
    return {
        setSelectedAppId(data) {
            dispatch(setTaskSelectedAppId(data))
        },
    }
}

export default connect(null, mapDispatchToProps)(TaskEdit);