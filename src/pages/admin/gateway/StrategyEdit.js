import React, {useEffect, useState} from "react"
import {Form, Input, Modal, message, Switch, TreeSelect} from "antd"
import TextArea from "antd/lib/input/TextArea"
import {saveOrUpdateStrategy} from "@/services/gateway/strategy";
import {getServerByAppId} from "@/services/gateway/app";
import {getFirstServerTree, getServerTree} from "@/utils/tools";
import {getStrategyById} from "../../../services/gateway/strategy";

const {SHOW_PARENT} = TreeSelect;

function StrategyEdit(props) {
    const {onOk, onCancel, appTreeData, selectedAppId, strategyId} = props
    const [currentStrategy, setCurrentStrategy] = useState(null)
    const [strategyOptionData, setStrategyOptionData] = useState([])

    const [form] = Form.useForm()
    useEffect(() => {
        if (selectedAppId && selectedAppId !==0) {
            getStrategyServerListByAppId(selectedAppId)
        }
        if (strategyId && strategyId !==0) {
            getStrategyById(strategyId).then(res => {
                console.log("getStrategyById>>>>>>", res.data.data)
                let obj = res.data.data
                obj.appId = selectedAppId
                let ss = []
                for (let i in obj.strategyServers) {
                    ss.push(obj.strategyServers[i].pk.server.id)
                }
                obj.strategyServers = ss
                setCurrentStrategy(obj)
            }).catch(er => {
                message.destroy()
                message.error("获取策略详细信息失败！")
            })
        }else {
            setCurrentStrategy({appId:selectedAppId})
        }
    }, [selectedAppId, strategyId])

    const getStrategyServerListByAppId = (appId) => {
        getServerByAppId(appId).then(res => {
            if (res.data.code === 200) {
                const templ = new Set()
                let sArr = res.data.data
                let options = []
                for (let i in sArr) {
                    let sObj = sArr[i]
                    if (!templ.has(sObj.module.project.id)) {
                        options.push({
                            title: sObj.module.project.projectName,
                            key: sObj.module.project.id,
                            value: sObj.module.project.id,
                            pid: 0
                        })
                    }
                    if (!templ.has(sObj.module.id)) {
                        options.push({
                            title: sObj.module.moduleName,
                            key: sObj.module.id,
                            value: sObj.module.id,
                            pid: sObj.module.project.id
                        })
                    }
                    if (!templ.has(sObj.id)) {
                        options.push({title: sObj.serverName, key: sObj.id, value: sObj.id, pid: sObj.module.id})
                    }
                    templ.add(sObj.module.project.id)
                    templ.add(sObj.module.id)
                    templ.add(sObj.id)
                }
                const result = getFirstServerTree(options)
                getServerTree(result, options)
                setStrategyOptionData(result)
            }
        }).catch(err => {
            message.destroy()
            message.error("获取授权应用的服务列表失败!")
        })
    }

    const handleOk = () => {
        form.validateFields().then(value => {
            // console.log("StrategyEdit>>>>handleOk::::", value)
            value.status = value.status ? 1 : 0
            const status = value.status ? 1 : 0;
            value.app = {id: value.appId};
            value.status = status;
            const ss = value.strategyServers
            if (ss) {
                let ssArr = []
                for (let i in ss) {
                    ssArr.push({
                        pk: {
                            server: {id: ss[i]},
                            strategy: {id: value.id}
                        }
                    })

                }
                value.strategyServers = ssArr
            }
            // console.log("StrategyEdit>>>>handleOk::::2222::::", value)
            saveOrUpdateStrategy(value).then(res => {
                if (res.data.code === 200) {
                    onOk(value)
                    message.destroy();
                    message.success("策略信息保存成功！");
                } else {
                    message.destroy();
                    message.error("策略保存接口异常!");
                }
            }).catch(err => {
                // console.log(err)
                message.destroy();
                message.error("策略保存失败!");
            })

        }).catch(err => {
            message.destroy()
            message.error("策略信息提交失败")
        })
    }

    //选择应用列表后，刷新服务列表
    const onChange = value => {
        getStrategyServerListByAppId(value)
        form.setFieldsValue({strategyServers:[]})
    };

    return <Modal title="编辑策略" visible={true}
                  onOk={handleOk}
                  onCancel={onCancel}
                  destroyOnClose={true}
                  maskClosable={false}
                  width="60%"
    >
        {currentStrategy ? <Form form={form} initialValues={currentStrategy}>
            <Form.Item name="id" hidden={true}>
                <Input/>
            </Form.Item>
            <Form.Item name="appId" label="所属应用" rules={[{required: true, message: "请选择应用"}]}>
                <TreeSelect
                    style={{width: '100%'}}
                    value={selectedAppId}
                    dropdownStyle={{maxHeight: 400, overflow: 'auto'}}
                    treeData={appTreeData}
                    placeholder="请选择上级应用"
                    treeDefaultExpandAll
                    onChange={onChange}/>
            </Form.Item>
            <Form.Item name="strategyName" label="策略名称" rules={[{required: true, message: "请输入策略名称"}]}>
                <Input/>
            </Form.Item>
            <Form.Item name="strategyServers" label="服务列表" rules={[{required: true, message: "请选择服务"}]}>
                <TreeSelect
                    style={{width: '100%'}}
                    treeData={strategyOptionData}
                    placeholder="请选择服务列表"
                    treeDefaultExpandAll
                    treeCheckable={true}
                    showCheckedStrategy={SHOW_PARENT}
                    />
            </Form.Item>
            <Form.Item name="status" label="&nbsp;&nbsp;&nbsp;策略状态" valuePropName="checked">
                <Switch/>
            </Form.Item>
            <Form.Item name="remark" label="&nbsp;&nbsp;&nbsp;策略备注">
                <TextArea/>
            </Form.Item>
        </Form> : null}
    </Modal>
}

export default StrategyEdit;