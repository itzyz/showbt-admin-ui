import {Button, Card, Table, Space, Tag, Popconfirm, message} from "antd";
import React, {useEffect, useState} from "react"
import {deleteAppById, getAppList} from "../../../services/gateway/app";
import AppEdit from "./AppEdit";
import {createAppTreeData, createServerTreeData} from "../../../utils/tools";
import {getAllProjectsAndServer} from "../../../services/gateway/project";

export default function AppList() {
    const [dataSource, setDataSource] = useState([]);
    const [appTreeData, setAppTreeData] = useState([]);
    const [appId, setAppId] = useState(null)
    const [serverTreeData, setServerTreeData] = useState([])

    useEffect(() => {
        refreshAppList()
        getAllProjectsAndServer().then(res => {
            let result = []
            createServerTreeData(result, res.data.data)
            setServerTreeData(result)
        }).catch(err => {
            message.destroy();
            message.error("服务列表获取失败！")
        })
    }, [])

    const refreshAppList = () => {
        getAppList().then(res => {
            let data = []
            createAppTreeData(data, res.data.data)
            setDataSource(res.data.data)
            setAppTreeData([{title: "顶级应用", key: 0, value: 0, children: data}])
        }).catch(err => {
            message.destroy()
            message.error("获取应用列表失败")
        })
    }

    const save = () => {
        refreshAppList()
        setAppId(null)
    }

    const onDelete = id => {
        deleteAppById(id).then(res => {
            message.destroy()
            message.success("应用删除成功")
            refreshAppList()
        }).catch(err => {
            message.destroy()
            message.error("应用删除失败！")
        })
    }

    const onUpdateClick = (id) => {
        setAppId(id)
    }

    const columns = [
        {
            key: "id",
            title: "序号",
            width: "80",
            align: "center",
            render: (txt, record, index) => record.id
        },
        {
            title: "应用名称",
            dataIndex: "appName",
        },
        {
            title: "授权码",
            dataIndex: "apikey"
        },
        {
            title: "应用状态",
            dataIndex: "status",
            render: (txt, record, index) => txt === 1 ? <Tag color="#87d068">正常</Tag> : <Tag color="#f50">禁用</Tag>
        },
        {
            title: "备注",
            dataIndex: "remark"
        },
        {
            title: "操作",
            width: "10%",
            render: (txt, record, index) =>
                <Space>
                    <Button size="small" type="primary"
                            onClick={() => {
                                onUpdateClick(record.id)
                            }}>修改</Button>
                    <Popconfirm title="确定要删除该应用吗？"
                                onConfirm={() => {
                                    onDelete(record.id);
                                }}
                                onCancel={() => {
                                    console.log("取消删除操作")
                                }}
                    >
                        <Button size="small" type="danger">删除</Button>
                    </Popconfirm>
                </Space>

        }
    ]
    return <Card title="应用列表" extra={<Button type="primary" onClick={() => {
        setAppId(0)
    }}>新增</Button>}>
        <Table rowKey={columns => columns.id} columns={columns} dataSource={dataSource}></Table>
        {appId != null ? <AppEdit
            appId={appId}
            onCancel={() => {
                setAppId(null)
            }}
            onOkSubmit={save}
            appTreeData={appTreeData}
            serverTreeData={serverTreeData}
        /> : null
        }
    </Card>
}