import {Table, Card, message, Tag, Button, Popconfirm} from "antd"

import React, {useEffect, useState} from "react";
import ModuleEditModal from "./ModuleEdit";
import {deleteModule, getModuleList, saveOrUpdateModule} from "@/services/gateway/module";
import {CONFIG} from "@/utils/config";

const pageSize = CONFIG.pageSize

function ModuleList(props) {
    let projectId = props.match.params.pid;
    if (props.location.query) {
        sessionStorage.setItem(CONFIG.KEY_PROJECT_LABEL_NAME, props.location.query.projectName)
    }
    let projectName = sessionStorage.getItem("project_label_name");

    // eslint-disable-next-line no-unused-vars
    const [confirmLoading, setConfirmLoading] = useState(false)
    const [dataSource, setDataSource] = useState(null)
    const [totalElements, setTotalElements] = useState(null)
    const [currentPage, setCurrentPage] = useState(1)
    const [moduleId, setModuleId] = useState(null)

    useEffect(() => {
        getModuleListByPage(1, pageSize)
    }, [])// eslint-disable-line react-hooks/exhaustive-deps

    const getModuleListByPage = (page, pageSize) => {
        getModuleList(projectId, page, pageSize).then(res => {
            if (res.data.code === 200) {
                setDataSource(res.data.data.content)
                setTotalElements(res.data.data.totalElements)
                setCurrentPage(page)
            }
        }).catch(err => {
            message.destroy()
            message.error("获取模块列表失败")
        })
    }

    //点击新增按钮时触发此事件
    const showModalAdd = () => {
        //清空表单默认值
        setModuleId(0)
    }

    //点击更新按钮时触发此事件
    const showModalUpdate = (id) => {
        setModuleId(id)

    }

    /**
     * 点击modal确认按钮后，提交表单数据，并刷新列表页
     * @param values
     */
    const saveModule = (values) => {
        saveOrUpdateModule({...values, project: {id: projectId}}).then(res => {
            setModuleId(null)
            // dispatch(loadModule({pid: projectId, page: module.page, size: module.pageSize}))
            getModuleListByPage(currentPage, pageSize)
        }).catch(err => {
            setModuleId(null)
            message.destroy()
            message.error("保存模块数据失败！！")
        })
    }

    const delModule = id => {
        deleteModule(id).then(res => {
            message.destroy()
            message.success("模块删除成功!")
            // dispatch(loadModule({pid: projectId, page: module.page, size: module.pageSize}));
            getModuleListByPage(currentPage, pageSize)
        })
    }

    const columns = [
        {
            title: "序号",
            key: "id",
            width: 80,
            align: "center",
            render: (txt, record, index) => index + 1
        },
        {
            title: "模块名称",
            dataIndex: "moduleName"
        },
        {
            title: "虚拟路径",
            dataIndex: "label"
        },
        {
            title: "真实URL",
            dataIndex: "realPath"
        },
        {
            title: "心跳地址",
            dataIndex: "health"
        },
        {
            title: "创建时间",
            dataIndex: "createTime"
        },
        {
            title: "状态",
            dataIndex: "status",
            render: (txt, record, index) => txt === 0 ? <Tag color="#f50">无效</Tag> : <Tag color="#87d068">有效</Tag>
        },
        {
            title: "操作",
            render: (txt, record, index) => {
                return <div>
                    <Button type="primary" size="small"
                            onClick={() => props.history.push({
                                pathname: `/admin/server/list/${projectId}/${record.id}`,
                                state: {
                                    projectId: projectId,
                                    projectName: projectName,
                                    moduleName: record.moduleName
                                }
                            })
                            }>服务</Button>
                    <Button type="primary" size="small"
                            style={{marginLeft: "1rem"}}
                            onClick={() => {
                                showModalUpdate(record.id)
                            }}
                    >编辑</Button>
                    <Popconfirm title="警告：删除模块时自动删除模块下的服务，确定要删除吗？"
                                onCancel={() => {
                                    message.info("取消删除操作")
                                }} onConfirm={() => delModule(record.id)}
                    >
                        <Button type="danger" size="small" style={{margin: "0 1rem"}}>删除</Button>
                    </Popconfirm>
                </div>
            }
        }
    ]

    return <Card title={`模块列表【${projectName}】`} extra={<div>
        <Button type="primary" onClick={showModalAdd}>新增</Button>
        <Button type="defalut" style={{marginLeft: "1 rem"}}
                onClick={() => props.history.push(`/admin/project`)}>返回</Button>
    </div>
    }>
        <Table rowKey={col => col.id}
               columns={columns}
               dataSource={dataSource}
               pagination={{
                   total: totalElements, defaultPageSize: 10, onChange: getModuleListByPage
               }}
        />
        {moduleId !== null ? <ModuleEditModal
            confirmLoading={confirmLoading}
            moduleId={moduleId}
            onCancel={() => {
                setModuleId(null)
            }}
            submitMap={(values) => {
                saveModule(values)
            }}>

        </ModuleEditModal> : null}
    </Card>
}

export default ModuleList