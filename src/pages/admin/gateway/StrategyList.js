import React, {useEffect, useState} from "react";
import {
    Button,
    Card,
    Form,
    message,
    Popconfirm,
    Space,
    Table,
    Tag,
    TreeSelect,
    Row,
    Col,
    Select,
} from "antd";
import StrategyEdit from "./StrategyEdit";
import {deleteStrategy, getStrategyList} from "../../../services/gateway/strategy";
import {getAppList} from "../../../services/gateway/app";
import {createAppTreeData} from "../../../utils/tools";
import {CONFIG} from "../../../utils/config";

const {Option} = Select
const pageSize = CONFIG.pageSize
const StrategyList = (props) => {
    const [strategyId, setStrategyId] = useState(null)
    const [appTreeData, setAppTreeData] = useState(null)
    const [dataSource, setDataSource] = useState(null)
    const [appId, setAppId] = useState(null)
    const [searchAppId, setSearchAppId] = useState(null)
    const [searchState, setSearchState] = useState(null)
    const [totalElements, setTotalElements] = useState(null)
    const [currentPage, setCurrentPage] = useState(1)

    const [form] = Form.useForm()
    useEffect(() => {
        getStrategyListByPage(1, 10);
    }, [searchAppId, searchState])// eslint-disable-line react-hooks/exhaustive-deps
    useEffect(() => {
        const refreshAppList = () => {
            getAppList().then(res => {
                let data = []
                createAppTreeData(data, res.data.data)
                setAppTreeData([{title: "顶级应用", key: 0, value: 0, children: data}])
            }).catch(err => {
                message.destroy()
                message.error("获取应用列表失败")
            })
        }

        refreshAppList();
    }, [])

    const getStrategyListByPage = (page) => {
        getStrategyList(searchAppId, searchState, page, pageSize).then(res => {
            setDataSource(res.data.data.content)
            setTotalElements(res.data.data.totalElements)
            setCurrentPage(page)
        }).catch(err => {
            message.destroy()
            message.error("获取任务列表失败")
        });
    }

    const searchStrategyList = () => {
        form.validateFields().then(values => {
            setSearchAppId(values.searchAppId)
            setSearchState(values.searchState)
        })
    }

    //保存新增策略后，重新获取策略列表
    const strategyHandleOk = () => {
        setAppId(null);
        setStrategyId(null);
        getStrategyListByPage();
    }

    //取消新增策略
    const strategyOnCancel = () => {
        setAppId(null)
        setStrategyId(null)
    }

    const onFinish = () => {

    }

    const columns = [
        {
            key: "id",
            title: "策略ID",
            width: "180px",
            align: "center",
            render: (txt, record) => record.id
        },
        {
            title: "策略名称",
            dataIndex: "strategyName"
        },
        {
            title: "应用名称",
            render: (txt, record) => record.app.appName
        },
        {
            title: "状态",
            render: (txt, record) => {
                if (record.status === 1) {
                    return <Tag color="#87d068">有效</Tag>
                } else {
                    return <Tag color="#f50">无效</Tag>
                }
            }
        }, {
            title: "操作",
            render: (txt, record, index) => {
                return <Space>
                    <Button type="primary" size="small" onClick={() => {
                        setStrategyId(record.id)
                        setAppId(record.app.id)
                    }}>修改</Button>
                    <Popconfirm title="确定要删除这个任务吗？"
                                onCancel={() => {
                                    message.info("取消删除操作！")
                                }}
                                onConfirm={() => {
                                    deleteStrategy(record.id).then(res => {
                                        message.destroy()
                                        message.success("策略删除成功！")
                                        getStrategyListByPage()
                                    }).catch(err => {
                                        message.destroy()
                                        message.error("策略删除失败！")
                                    })
                                }}
                    >
                        <Button type="danger" size="small">删除</Button>
                    </Popconfirm>
                </Space>
            }
        }
    ]

    return <div>
        <Card title="任务列表" extra={<Button type="primary" onClick={() => {
            setStrategyId(0)
            setAppId(0)
        }}>新增</Button>}>
            <Form
                form={form}
                name="advanced_search"
                className="ant-advanced-search-form"
                onFinish={onFinish}
                initialValues={{searchAppId:0,searchState:0}}
            >
                <Row gutter={24}>
                    <Col span={6} key={1}>
                        <Form.Item name="searchAppId" label="选择应用">
                            <TreeSelect
                                treeData={appTreeData}
                                treeDefaultExpandAll={true}
                                onChange={searchStrategyList}
                            />
                        </Form.Item>
                    </Col>
                    <Col span={4} key={2}>
                        <Form.Item name="searchState" label="策略状态">
                            <Select onChange={searchStrategyList}>
                                <Option value={0}>所有</Option>
                                <Option value={2}>有效</Option>
                                <Option value={1}>无效</Option>
                            </Select>
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
            <Table rowKey={columns => columns.id}
                   columns={columns}
                   dataSource={dataSource}
                   pagination={{
                       total: totalElements,
                       defaultPageSize: pageSize,
                       onChange: getStrategyListByPage
                   }}
            ></Table>

            {strategyId != null ? <StrategyEdit
                strategyId={strategyId}
                appTreeData={appTreeData}
                onCancel={() => {
                    setStrategyId(null)
                    setAppId(null)
                }}
                onOk={() => {
                    setStrategyId(null)
                    setAppId(null)
                    getStrategyListByPage(currentPage)
                }}
            >
            </StrategyEdit> : null}
        </Card>

        {strategyId && appId ?
            <StrategyEdit
                onOk={strategyHandleOk}
                onCancel={strategyOnCancel}
                appTreeData={appTreeData}
                selectedAppId={appId}
                strategyId={strategyId}
                destroyOnClose={true}
                maskClosable={false}
            >
            </StrategyEdit> : null
        }
    </div>
}

export default StrategyList;