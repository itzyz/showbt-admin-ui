import {Input, message, Modal, Switch} from "antd";
import {Form} from "antd";
import React, {useEffect, useState} from "react";
import {getModuleById} from "../../../services/gateway/module";

/**
 * https://coding.imooc.com/learn/questiondetail/174230.html
 * @param visible
 * @param submitMap
 * @param onCancel
 * @param currentDetailData
 * @returns {JSX.Element}
 * @constructor
 */
const ModuleEditModal = (props) => {
    const {confirmLoading, submitMap, onCancel, moduleId} = props
    // const formRef = React.createRef();
    const [form] = Form.useForm();

    const [currentModule, setCurrentModule] = useState(null)
    useEffect(() => {
        if (moduleId !== 0) {
            getModuleById(moduleId).then(res => {
                setCurrentModule(res.data.data)
            }).catch(err => {
                setCurrentModule({})
                message.destroy()
                message.error("获取模块详细信息失败！")
            })
        } else {
            setCurrentModule({})
        }
    },[moduleId])

    const handleOk = () => {
        form.validateFields().then(values => {
            if (values.status) {
                values.status = 1
            } else {
                values.status = 0
            }
            submitMap(values)
        }).catch(err => {
            message.destroy()
            message.error("保存模块信息失败")
        })
    }

    return <Modal title="编辑模块" visible={true}
                  onOk={handleOk}
                  confirmLoading={confirmLoading}
                  onCancel={onCancel}
                  destroyOnClose={true}
                  maskClosable={false}
                  width="50%"
    >
        {currentModule ? <Form form={form} initialValues={currentModule}>
            <Form.Item hidden="true" name="id">
                <Input/>
            </Form.Item>
            <Form.Item name="moduleName" label="模块名称" rules={[{required: true, message: "请输入模块名称！"}]}>
                <Input placeholder="请输入模块名称"/>
            </Form.Item>
            <Form.Item name="label" label="虚拟路径" rules={[{required: true, message: "请输入虚拟路径！"}]}>
                <Input placeholder="请输入虚拟路径"/>
            </Form.Item>
            <Form.Item name="realPath" label="真实 URL" rules={[{required: true, message: "请输入真实URL！"}]}>
                <Input placeholder="请输入真实URL"/>
            </Form.Item>
            <Form.Item name="health" label="&nbsp;&nbsp;&nbsp;心跳地址">
                <Input placeholder="请输入心跳地址"/>
            </Form.Item>
            <Form.Item label="&nbsp;&nbsp;&nbsp;模块状态" name="status" valuePropName="checked">
                <Switch/>
            </Form.Item>
            <Form.Item name="remark" label="&nbsp;&nbsp;&nbsp;备注">
                <Input.TextArea placeholder="请输入备注"/>
            </Form.Item>
        </Form> : null}
    </Modal>
}

export default ModuleEditModal;