import React, {useEffect, useState} from 'react'
import {Button, Card, message, Popconfirm, Space, Table} from "antd";
import {CONFIG} from "../../../utils/config";
import {deleteClient, getClientPage} from "../../../services/upms/clientApi";
import ClientEdit from "./ClientEdit";

const pageSize = CONFIG.pageSize

function Client() {
    const [dataSource, setDataSource] = useState(null)
    const [totalElements, setTotalElements] = useState(null)
    const [currentPage, setCurrentPage] = useState(1)
    const [clientId, setClientId] = useState(null)

    useEffect(() => {
        getClientListByPage(0, pageSize)
    }, [])

    const getClientListByPage = (page, pageSize) => {
        getClientPage(page, pageSize).then(res => {
            setDataSource(res.data.data.records)
            setTotalElements(res.data.data.total)
            setCurrentPage(page)
        }).catch(err => {
            message.destroy()
            message.error("获取终端列表失败")
        })
    }

    const columns = [
        {
            key: "id",
            title: "终端ID",
            width: "80",
            align: "center",
            render: (txt, record, index) => record.clientId
        },
        {
            title: "终端密钥",
            dataIndex: "clientSecret"
        },
        {
            title: "作用范围",
            dataIndex: "scope"
        },
        {
            title: "授权方式",
            dataIndex: "authorizedGrantTypes"
        },
        {
            title: "跳转地址",
            dataIndex: "webServerRedirectUri"
        },
        {
            title: "操作",
            render: (txt, record, index) => {
                return <Space>
                    <Button type="primary" size="small" onClick={() => {
                        setClientId(record.clientId)
                    }}>修改</Button>
                    <Popconfirm title="确定要删除这个终端吗？"
                                onCancel={() => {
                                    message.info("取消删除操作！")
                                }}
                                onConfirm={() => {
                                    deleteClient(record.clientId).then(res=>{
                                        message.destroy()
                                        message.success("终端删除成功！")
                                        getClientListByPage(currentPage, pageSize)
                                    }).catch(err=>{
                                        message.destroy()
                                        message.error("删除失败")
                                    })
                                }}
                    >
                        <Button type="danger" size="small">删除</Button>
                    </Popconfirm>
                </Space>
            }
        }
    ]
    return (
        <Card title="终端管理" extra={<Button type="primary" onClick={() => {
            setClientId("0")
        }}>新增</Button>}>
            <Table
                rowKey={col => col.clientId}
                columns={columns}
                dataSource={dataSource}
                pagination={{
                    total: totalElements,
                    defaultPageSize: pageSize,
                    onChange: getClientListByPage
                }}

            />
            {
                clientId ?
                    <ClientEdit
                        resetModal={() => {
                            setClientId(null)
                            getClientListByPage(currentPage, pageSize)
                        }}
                        clientId={clientId}
                    /> : null
            }
        </Card>
    )
}

export default Client