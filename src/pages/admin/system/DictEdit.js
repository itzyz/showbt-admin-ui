import {Form, Input, message, Modal} from "antd";
import React, {useEffect, useState} from "react";
import {getDictById, saveOrUpdateDict} from "../../../services/upms/dictApi";

function DictEdit(props) {
    const {resetModal, dictId} = props
    const [currentDict, setCurrentDict] = useState(null)
    useEffect(() => {
        if (dictId && dictId !== 0) {
            getDictById(dictId).then(res => {
                setCurrentDict(res.data.data)
            }).catch(err => {
                message.destroy()
                message.error("获取字典详细信息失败！")
            })
        } else {
            setCurrentDict({})
        }
    }, [dictId])

    const onSaveDict = ()=>{
        form.validateFields().then(values => {
            saveOrUpdateDict(values).then(res => {
                if (res.status === 200) {
                    message.destroy()
                    message.success("字典信息保存成功！")
                    resetModal()
                } else {
                    message.destroy()
                    message.error("字典信息保存失败！")
                }
            }).catch(err => {
                message.destroy()
                message.error("字典信息保存接口异常！")
            })
        }).catch(err=>{
            message.destroy()
            message.error("字典信息保存异常！")
        })
    }

    const [form] = Form.useForm()
    return <Modal title="编辑字典" visible={true}
                  onOk={onSaveDict}
                  onCancel={resetModal}
                  destroyOnClose={true}
                  maskClosable={false}
                  width="50%"
    >
        {currentDict ? <Form form={form} initialValues={currentDict}>
            <Form.Item hidden="true" name="id">
                <Input/>
            </Form.Item>
            <Form.Item name="label" label="字典名称" rules={[{required: true, message: "请输入字典名称！"}]}>
                <Input placeholder="请输入字典名称"/>
            </Form.Item>
            <Form.Item name="type" label="字典类型" rules={[{required: true, message: "请输入字典类型！"}]}>
                <Input placeholder="请输入字典类型"/>
            </Form.Item>
            <Form.Item name="value" label="字典键值" rules={[{required: true, message: "请输入字典键值！"}]}>
                <Input placeholder="请输入字典键值"/>
            </Form.Item>
            <Form.Item name="sort" label="&nbsp;&nbsp;&nbsp;字典排序">
                <Input placeholder="请输入字典排序" value={0}/>
            </Form.Item>
            <Form.Item name="description" label="&nbsp;&nbsp;&nbsp;字典描述">
                <Input placeholder="请输入字典描述"/>
            </Form.Item>
            <Form.Item name="remarks" label="&nbsp;&nbsp;&nbsp;字典备注">
                <Input.TextArea placeholder="请输入备注"/>
            </Form.Item>
        </Form> : null}
    </Modal>
}

export default DictEdit