import React, {useEffect, useState} from 'react'
import {Card, message, Table} from "antd";
import {getLogPage} from "../../../services/upms/logApi";
import {CONFIG} from "../../../utils/config";

const pageSize = CONFIG.pageSize

function Log() {
    const [dataSource, setDataSource] = useState(null)
    const [totalElements, setTotalElements] = useState(null)
    // const [currentPage, setCurrentPage] = useState(1)

    useEffect(() => {
        getLogListByPage(1,pageSize)
    }, [])

    const getLogListByPage = (page, pageSize) =>{

        console.log(".............", page)

        getLogPage(page, pageSize).then(res => {
            const dictRes = res.data.data
            setDataSource(dictRes.records)
            setTotalElements(dictRes.total)
        }).catch(err => {
            message.destroy()
            message.error("日志列表加载失败")
        })
    }

    const columns = [
        {
            key: "id",
            title: "编号",
            width: "80",
            align: "center",
            render: (txt, record, index) => record.id
        },
        {
            title: "日志名称",
            dataIndex: "title",
        },
        {
            title: "日志类型",
            dataIndex: "type",
        },
        {
            title: "字典名称",
            dataIndex: "createBy",
        },
        {
            title: "创建时间",
            dataIndex: "createTime",
        },
        {
            title: "操作IP",
            dataIndex: "remoteAddr",
        },
        {
            title: "用户代理",
            dataIndex: "userAgent",
        },
        {
            title: "请求URI",
            dataIndex: "requestUri",
        },
        {
            title: "操作方式",
            dataIndex: "method",
        },
        // {
        //     title: "操作数据",
        //     dataIndex: "params",
        // },
        // {
        //     title: "异常信息",
        //     dataIndex: "exception",
        // },
        {
            title: "服务ID",
            dataIndex: "serviceId",
        },
    ]
    return (
        <Card title="日志管理">
            <Table
                rowKey={col => col.id}
                columns={columns}
                dataSource={dataSource}
                pagination={{
                    total: totalElements,
                    defaultPageSize: pageSize,
                    onChange: getLogListByPage
                }}
            />
        </Card>
    )
}

export default Log