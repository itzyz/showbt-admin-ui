import React, {useEffect, useState} from 'react'
import {getDictPage} from "@/services/upms/dictApi";
import {Table, message, Space, Button, Popconfirm, Card} from "antd";
import DictEdit from "./DictEdit";
import {CONFIG} from "../../../utils/config";
const pageSize = CONFIG.pageSize

function Dict() {
    const [dataSource, setDataSource] = useState(null)
    const [dictId, setDictId] = useState(null)
    const [totalElements, setTotalElements] = useState(null)
    // const [currentPage, setCurrentPage] = useState(1)
    useEffect(() => {
        getDictListByPage(1,pageSize)
    }, [])

    const getDictListByPage = (page, pageSize) =>{
        getDictPage(page, pageSize).then(res => {
            if (res.data.code === 200) {
                const dictRes = res.data.data
                console.log(dictRes)
                setDataSource(dictRes.records)
                setTotalElements(dictRes.total)
                // setCurrentPage(page)
            }
        }).catch(err => {
            message.destroy()
            message.error("获取字典列表失败")
        })
    }

    const columns = [
        {
            key: "id",
            title: "字典主键",
            width: "80",
            align: "center",
            render: (txt, record, index) => record.id
        },
        {
            title: "字典名称",
            dataIndex: "label",
        },
        {
            title: "字典类型",
            dataIndex: "type"
        },
        {
            title: "字典键值",
            dataIndex: "value"
        },
        {
            title: "字典排序",
            dataIndex: "sort"
        },
        {
            title: "字典描述",
            dataIndex: "description"
        },
        {
            title: "字典备注",
            dataIndex: "remarks"
        },
        {
            title: "创建时间",
            dataIndex: "createTime"
        },
        {
            title: "修改时间",
            dataIndex: "updateTime"
        },
        {
            title: "字典状态",
            dataIndex: "delFlag"
        },
        {
            title: "操作",
            width: "10%",
            render: (txt, record, index) =>
                <Space>
                    <Button size="small" type="primary"
                            onClick={() => {
                                setDictId(record.id)
                            }}>修改</Button>
                    <Popconfirm title="确定要删除该应用吗？"
                                onConfirm={() => {
                                }}
                                onCancel={() => {
                                    message.warn("取消删除操作")
                                }}
                    >
                        <Button size="small" type="danger">删除</Button>
                    </Popconfirm>
                </Space>

        }
    ]

    return (
        <Card title="字典管理" extra={<Button type="primary" onClick={() => {
            setDictId(0)
        }}>新增</Button>}>
            <Table
                columns={columns}
                dataSource={dataSource}
                rowKey={columns => columns.id}
                pagination={{
                    total: totalElements,
                    defaultPageSize: pageSize,
                    onChange: getDictListByPage
                }}
            />
            {dictId != null ? <DictEdit
                    resetModal={() => {
                        setDictId(null)
                    }}
                    dictId={dictId}
                />
                : null
            }
        </Card>
    )
}

export default Dict