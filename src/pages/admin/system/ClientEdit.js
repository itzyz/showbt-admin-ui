import {Form, Input, message, Modal, Select} from "antd";
import React, {useEffect, useState} from "react";
import {getClientById, saveOrUpdateClient} from "../../../services/upms/clientApi";

const {Option} = Select

function ClientEdit(props) {
    const {resetModal, clientId} = props
    const [currentClient, setCurrentClient] = useState(null)
    useEffect(() => {
        if (clientId && clientId !== "0") {
            getClientById(clientId).then(res => {
                const obj = res.data.data
                obj.id = obj.clientId
                setCurrentClient(obj)
            }).catch(err => {
                message.destroy()
                message.error("获取终端详细信息失败！")
            })
        } else {
            setCurrentClient({})
        }
    }, [clientId])

    const onSaveClient = () => {
        form.validateFields().then(values => {
            saveOrUpdateClient(values).then(res => {
                if (res.status === 200) {
                    message.destroy()
                    message.success("终端信息保存成功！")
                    resetModal()
                } else {
                    message.destroy()
                    message.error("终端信息保存失败！")
                }
            }).catch(err => {
                message.destroy()
                message.error("终端信息保存接口异常！")
            })
        }).catch(err=>{
            message.destroy()
            message.error("终端信息保存异常！")
        })
    }

    const [form] = Form.useForm()
    return <Modal title="编辑终端" visible={true}
                  onOk={onSaveClient}
                  onCancel={resetModal}
                  destroyOnClose={true}
                  maskClosable={false}
                  width="50%"
    >
        {currentClient ? <Form form={form} initialValues={currentClient}>
            <Form.Item hidden="true" name="id">
                <Input/>
            </Form.Item>
            <Form.Item name="clientId" label="终&nbsp;&nbsp;端&nbsp;&nbsp;ID" rules={[{required: true, message: "请输入终端ID！"}]} >
                {currentClient.clientId?<Input disabled={true}/>:<Input />}
            </Form.Item>
            <Form.Item name="clientSecret" label="终端密钥" rules={[{required: true, message: "请输入终端密钥！"}]}>
                <Input placeholder="请输入终端名称"/>
            </Form.Item>
            <Form.Item name="scope" label="作&nbsp;&nbsp;用&nbsp;&nbsp;域" rules={[{required: true, message: "请输入作用域！"}]}>
                <Input placeholder="请输入作用域"/>
            </Form.Item>
            <Form.Item name="authorizedGrantTypes" label="&nbsp;&nbsp;&nbsp;授权方式">
                <Input placeholder="请输入授权方式"/>
            </Form.Item>
            <Form.Item name="webServerRedirectUri" label="&nbsp;&nbsp;&nbsp;回调地址">
                <Input placeholder="请输入回调地址"/>
            </Form.Item>
            <Form.Item name="autoApprove" label="&nbsp;&nbsp;&nbsp;自动放行">
                <Select placeholder="是否自动放行">
                    <Option value="true">是</Option>
                    <Option value="false">否</Option>
                </Select>
            </Form.Item>
            <Form.Item name="resourceIds" label="&nbsp;&nbsp;&nbsp;资&nbsp;&nbsp;源&nbsp;&nbsp;ID">
                <Input placeholder="请输入资源ID"/>
            </Form.Item>
            <Form.Item name="authorities" label="&nbsp;&nbsp;&nbsp;权&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;限">
                <Input.TextArea placeholder="请输入权限"/>
            </Form.Item>
            <Form.Item name="accessTokenValidity" label="&nbsp;&nbsp;&nbsp;令牌有效时间">
                <Input placeholder="请输入令牌有效时间"/>
            </Form.Item>
            <Form.Item name="refreshTokenValidity" label="&nbsp;&nbsp;&nbsp;令牌刷新时间">
                <Input placeholder="请输入令牌刷新时间"/>
            </Form.Item>
            <Form.Item name="additionalInformation" label="&nbsp;&nbsp;&nbsp;扩展信息">
                <Input placeholder="请输入扩展信息"/>
            </Form.Item>
        </Form> : null}
    </Modal>
}

export default ClientEdit