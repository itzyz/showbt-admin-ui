import React from 'react'

import {Card, Table, Button, Popconfirm} from 'antd'
import {connect} from "react-redux";
import loadProduct from "../../../store/actions/product";

function ProductList(props) {
    const columns = [{
        title: "序号",
        key: "id",
        width: 80,
        align: "center",
        render: (txt, record, index) => index + 1
    },
        {
            title: "商品名称",
            dataIndex: "name"
        },
        {
            title: "商品价格",
            dataIndex: "price"
        },
        {
            title: "操作",
            render: (txt, record, index) => {
                return (
                    <div>
                        <Button type="primary" size="small">修改</Button>
                        <Popconfirm title="确定要删除此项吗？"
                                    onCancel={() => console.log("用户取消删除")}
                                    onConfirm={() => {
                                        console.log("用户确认删除")
                                    }}>
                            <Button type="danger" size="small" style={{margin: "0 1rem"}}>删除</Button>
                        </Popconfirm>
                    </div>
                )
            }
        }
    ]
    return (
        <Card title="商品列表"
              extra={<Button type="primary" onClick={() => props.history.push("/admin/products/edit")}>新增</Button>}>
            <Table columns={columns} bordered dataSource={props.product.dataSource} rowKey={columns => columns.id}/>
        </Card>
    )
}

const mapStateToProps= (state,ownProps)=>{
    console.log("state::::", ownProps)
    return {product: state.products}
}

const mapDispatchToProps=(dispatch, ownProps) =>{
    console.log("dispatch::::", ownProps)
    return {
        dataSource: dispatch(loadProduct({
                        abc: "aaa",
                        total: 0
                    }))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList)
