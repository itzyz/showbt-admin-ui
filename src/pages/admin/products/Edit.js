import React from 'react'

import {Form, Card, Input, Button, InputNumber} from "antd"

function Edit(props) {
    console.log(props)
    const onFinish = values=>{
        console.log('Success:',values)
    }
    const onFinishFail = errorInfo=>{
        console.log("Fail:", errorInfo)
    }
    //此处为自定义表单验证代码
    const priceValidator = (rule, value, callback)=>{
        if(value * 1 > 100){
            callback("价格不能大于100");
        }else {
            callback();
        }
    }
    return (
        <Card title="添加商品">
            <Form onFinish={onFinish} onFinishFailed={onFinishFail}>
                <Form.Item label="商品名称" name="productName" rules={[{ required: true, message: '请求输入商品名称!' }]} >
                    <Input placeholder="请输入商品名称"/>
                </Form.Item>
                <Form.Item label="商品价格" name="productPrice" rules={[{ required: true, message: '请求输入商品价格!' },{validator:priceValidator}]}>
                    <InputNumber placeholder="请输入商品价格"/>
                </Form.Item>
                <Form.Item><Button type="primary" htmlType="submit">保存</Button></Form.Item>
            </Form>
        </Card>
    )
}

export default Edit
