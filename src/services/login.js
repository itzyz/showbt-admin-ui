import { get, loginInstance } from "../utils/request"
import CryptoJs from 'crypto-js';
import Querystring from "query-string"

const scope = 'server'
const grant_type = 'password'
const secretKey = 'ThanksDataCenter';

/**
 * 给内容进行加密，生成16位密文
 * @param content  需要加密的内容
 * @returns {string}
 */
function enc(content) {
    let key = CryptoJs.enc.Latin1.parse(secretKey)
    // 加密
    return CryptoJs.AES.encrypt(
        content,
        key, {
        iv: key,
        mode: CryptoJs.mode.CBC,
        padding: CryptoJs.pad.ZeroPadding
    }).toString()
}

export const loginByUsername = (username, password, code, randomStr) => {
    //由于spring 的TokenEndpoint以@RequestParam接收参数，参数不能以json格式发送
    let data = Querystring.stringify({ username, password: enc(password), randomStr, code, grant_type, scope });
    return loginInstance.post('/auth/oauth/token', data, {
        headers: {
            //showbt:showbt
            'Authorization': 'Basic c2hvd2J0OnNob3didA=='
        }
    })
}

export const validateCode = () => {
    return get("/code");
}

export const loginOutAndCleanToken = (token)=>{
    return loginInstance.delete('/auth/oauth/logout',{
        headers:{
            'Authorization': token
        }
    })
}