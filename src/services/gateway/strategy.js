import {del, get, patch, post} from "../../utils/request";

export function getStrategyList(appId,state, page=1, size=10){
    console.log("strategy.js>>>getStrategyList>>>", state)
    return get('/spms/strategy/search-page',{page, size, "app.id":appId?appId:null, "status":state?state-1:null})
}

export function getStrategyById(id) {
    return get(`/spms/strategy/find-by-id/${id}`)
}

export function saveOrUpdateStrategy(values){
    if (values && values.id){
        console.log("update", values);
        return patch(`/spms/strategy/partial-update/${values.id}`, values);
    }else{
        console.log("save", values);
        return post('/spms/strategy/save', values);
    }
}

export function deleteStrategy(id) {
    return del(`/spms/strategy/remove/${id}`)
}