import {del, get, patch, post} from "../../utils/request";

export function getModuleList(pid, page=1, size=10){
    return get('/spms/module/search-page',{page, size, "project.id":pid})
}

export function getModuleById(mid) {
    return get(`/spms/module/find-by-id/${mid}`)
}

export function saveOrUpdateModule(values){
    if (values && values.id){
        console.log("update", values);
        return patch(`/spms/module/partial-update/${values.id}`, values);
    }else{
        console.log("save", values);
        return post('/spms/module/save', values);
    }
}

export function deleteModule(id) {
    return del(`/spms/module/remove/${id}`)
}