import {del, get, patch, post} from "../../utils/request";

export function getServerList(mid, page=1, size=10){
    return get('/spms/server/search-page',{page, size, "module.id":mid})
}

export function getServerById(id) {
    return get(`/spms/server/find-by-id/${id}`)
}

export function saveOrUpdateServer(values){
    if (values && values.id){
        console.log("update", values);
        return patch(`/spms/server/partial-update/${values.id}`, values);
    }else{
        console.log("save", values);
        return post('/spms/server/save', values);
    }
}

export function deleteServer(id) {
    return del(`/spms/server/remove/${id}`)
}