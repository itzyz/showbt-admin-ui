import {get, post, del} from "../../utils/request";

export const getTaskList = (appId, page = 1, pageSize = 10) => {
    return get("/spms/tasks/search-page", {page, pageSize, "app.id": appId ? appId : null})
}

export const saveOrUpdateTask = (value) => {
    return post("/spms/tasks/save", value)
}

export const getTaskById = (id) => {
    return get(`/spms/tasks/find-by-id/${id}`)
}

export const deleteTaskById = (id) => {
    return del(`/spms/tasks/remove/${id}`)
}