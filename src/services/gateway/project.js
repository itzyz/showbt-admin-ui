import {get, post, patch, del} from "../../utils/request";

/**
 * 获取项目列表
 * @returns {Promise<AxiosResponse<any>>}
 */
export function getProjectList(page = 1, pageSize = 10) {
    return get('/spms/project/page', {page, size: pageSize})
}

export function getProjectById(id) {
    return get(`/spms/project/find-by-id/${id}`)
}

export function saveOrUpdateProject(project) {
    if (project && project.id) {
        console.log("update", project);
        return patch(`/spms/project/partial-update/${project.id}`, project);
    } else {
        console.log("save", project);
        return post('/spms/project/save', project);
    }
}

export function deleteProject(id) {
    return del(`/spms/project/remove/${id}`)
}

export function getAllProjectsAndServer() {
    return get("/spms/project/getAllProjects")
}