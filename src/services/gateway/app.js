import {get, post, del} from "../../utils/request";

export const getAppList = () => {
    return get("/spms/app/root")
}

export const saveOrUpdateApp = (value) => {
    return post("/spms/app/save", value)
}

export const getAppById = (id) => {
    return get(`/spms/app/find-by-id/${id}`)
}

export const deleteAppById = (id) =>{
    return del(`/spms/app/remove/${id}`)
}

export const getServerByAppId = (appId)=>{
    return get(`/spms/app/getServersByAppId?appId=${appId}`)
}