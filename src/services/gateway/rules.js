import {get} from "../../utils/request";

export const getAllRules  = (page = 1, pageSize = 100)=>{
    return get("/spms/rules/page",{page, pageSize})
}