import {del, get} from "../../utils/request"

export function getLogPage(page = 1, pageSize = 10) {
    return get(`/admin/log/page`, {current:page, size:pageSize})
}

export function deleteLog(id) {
    return del(`/admin/log/delete/${id}`)
}