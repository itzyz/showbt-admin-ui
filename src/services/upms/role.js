import {del, get, post, put} from "../../utils/request"
import Querystring from "query-string";

export function getRoleList() {
    return get("/admin/role/list")
}

export function getRoleById(id) {
    return get(`/admin/role/index/${id}`)
}

export function updateOrSaveRole(values) {
    if (values && values.roleId) {
        return put("/admin/role/index", values)
    } else {
        return post("/admin/role/index", values)
    }
}

export function deleteRole(id){
    return del(`/admin/role/index/${id}`)
}

export const getMenuTreeByRoleId=(roleId)=>{
    return get(`/admin/menu/tree/${roleId}`)
}

export const saveMenuTreeByRoleId=(roleId, menuIds)=>{
    console.log("saveMenuTreeByRoleId:::menuIds,roleId::::", menuIds, roleId)
    let data = Querystring.stringify({roleId:parseInt(roleId), menuIds:menuIds});
    return put("/admin/role/updateMenu",data)
}

