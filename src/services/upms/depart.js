import {get, post, put} from "../../utils/request";

export const getDepartTree = ()=>{
    return get("/admin/dept/tree")
}

export const getDepartById = (id)=>{
    return get(`/admin/dept/index/${id}`)
}

export function saveOrUpdateDepart(values){
    console.log("aaaa::", values)
    if (values && values.deptId){
        return put(`/admin/dept/index`, values);
    }else{
        return post('/admin/dept/index', values);
    }
}