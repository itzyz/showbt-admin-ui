import {del, get, post, put} from "../../utils/request"

export function getClientById(id) {
    return get(`/admin/client/getById/${id}`)
}

export function getClientPage(page = 1, pageSize = 10) {
    return get(`/admin/client/page`, {current: page, size: pageSize})
}

export function saveOrUpdateClient(values) {

    console.log("saveOrUpdateClient>>>>", values)

    if (values && values.id) {
        return put(`/admin/client/update`, values);
    } else {
        return post('/admin/client/save', values);
    }
}

export function deleteClient(id) {
    return del(`/admin/client/delete/${id}`)
}