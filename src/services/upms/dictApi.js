import {del, get, post, put} from "../../utils/request"

export function getDictById(id) {
    return get(`/admin/dict/getById/${id}`)
}

export function getDictPage(page = 1, pageSize = 10) {
    return get(`/admin/dict/page`, {current: page, size: pageSize})
}

export function saveOrUpdateDict(values) {
    if (values && values.id) {
        return put(`/admin/dict/update`, values);
    } else {
        return post('/admin/dict/save', values);
    }
}

export function deleteDict(id, type) {
    return del(`/admin/dict/delete/${id}/${type}`)
}