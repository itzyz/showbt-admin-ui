import {del, get, post, put} from "../../utils/request"

export default function getUserPage(page, pageSize) {
    return get(`/admin/user/page`, {page, pageSize})
}

export function getUserById(userId){
    return get(`/admin/user/index/${userId}`)
}

export function saveOrUpdateUser(values) {
    if (values && values.userId) {
        return put(`/admin/user/index`, values);
    } else {
        return post('/admin/user/index', values);
    }
}

export function deleteUser(id) {
    return del(`/admin/user/index/${id}`)
}
