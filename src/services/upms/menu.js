import {del, get, post, put} from "../../utils/request"

const getCurrentUsersMenu = ()=>{
    return get("/admin/menu/index")
}

export const getMenuTree = ()=>{
    return get("/admin/menu/tree")
}

export function getMenuById(id) {
    return get(`/admin/menu/index/${id}`)
}

export function saveOrUpdateMenu(values){
    if (values && values.menuId){
        return put(`/admin/menu/index`, values);
    }else{
        return post('/admin/menu/index', values);
    }
}

export function deleteMenu(id) {
    return del(`/admin/menu/index/${id}`)
}

export default getCurrentUsersMenu;