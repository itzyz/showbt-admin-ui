//微服务管理-服务列表
export const LOAD_SERVER_LIST = "servers/loadServerList"
export const SET_SERVER_ID = "servers/setServerId"

export const LOAD_PROJECT_LIST = "project/loadProjectList"
export const SET_PROJECT_NAME = "project/setProjectName"

export const SET_TASK_SELECTED_APP_ID = "task/setTaskSelectedAppId"