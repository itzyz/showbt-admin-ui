import {createStore, compose,combineReducers, applyMiddleware} from "redux"
import thunk from "redux-thunk"
import productReducer from "./reduces/product";
import projectReducer from "./reduces/projectReducer";
import moduleReducer from "./reduces/module";
import serverReducer from "./reduces/serverReducer";
import MenuReducer from "./reduces/menu";
import taskReducer from "./reduces/taskReducer";

// import { combineReducers as cr } from 'redux-immutable'

const rootReducer = combineReducers({
    products: productReducer,
    projects: projectReducer,
    modules: moduleReducer,
    servers: serverReducer,
    menus: MenuReducer,
    taskReducer: taskReducer,
});



const composeEnhancers = typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose

const enhancer = composeEnhancers(
    applyMiddleware(thunk)
);

// export default createStore(rootReducer, compose(applyMiddleware(thunk)));
export default createStore(rootReducer, enhancer);