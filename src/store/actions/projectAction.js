import {getProjectList} from "../../services/gateway/project"
import {LOAD_PROJECT_LIST, SET_PROJECT_NAME} from "../constant";

const loadProject = payload => async dispatch => {
    const projectList = await getProjectList(payload.page, payload.pageSize);
    dispatch({
        type: LOAD_PROJECT_LIST,
        payload: projectList.data
    })
}

export const setProjectInfo = data => ({
    type: SET_PROJECT_NAME,
    data
})

export default loadProject;