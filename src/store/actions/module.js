import {getModuleList} from "../../services/gateway/module";

const loadModule=payload=> async dispatch=>{
    const {pid, page, size} = payload
    let res = await getModuleList(pid, page, size);
    dispatch({
        type:"LOAD_MODULE",
        payload: res.data
    })
}

export default loadModule;