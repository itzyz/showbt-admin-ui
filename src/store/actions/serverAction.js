import {getServerList} from "../../services/gateway/server";
import {SET_SERVER_ID, LOAD_SERVER_LIST} from "../constant";

const loadServers = payload => async dispatch => {
    const {mid, page, size} = payload
    let res = await getServerList(mid, page, size)
    dispatch({
        type: LOAD_SERVER_LIST,
        payload: res.data
    })
}

//存储serverId
export const setServerId = data => ({
    type: SET_SERVER_ID,
    data
})

export default loadServers;