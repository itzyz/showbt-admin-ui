import getCurrentUsersMenu from "../../services/upms/menu";

const loadMenu = payload => async dispatch => {
    let res = await getCurrentUsersMenu(payload)
    dispatch({
        type: "LOAD_MENU",
        payload: res.data
    })
}

export default loadMenu;