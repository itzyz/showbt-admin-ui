//存储serverId
import {SET_TASK_SELECTED_APP_ID} from "../constant";

export const setTaskSelectedAppId = data => ({
    type: SET_TASK_SELECTED_APP_ID,
    data
})