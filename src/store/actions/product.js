import {listApi} from "../../services/product";

/**
 *
 * @param payload 接收的外部参数
 * @returns {function(...[*]=)}
 */
const loadProduct = payload => async dispatch => {
    console.log("action::>>>payload>>>", payload)
    const res = await listApi();
    //当异步操作加载完成后，通过dispatch触发reducer改变数据
    dispatch({
        type:"PRODUCT_LOADED",
        payload: res
    })
}
export default loadProduct