import {fromJS} from 'immutable'
import {SET_TASK_SELECTED_APP_ID} from "../constant";

const defaultState = fromJS({
    selectedAppId: "",
})

const setAppId = (state, action) => {
    return state.merge({
        selectedAppId: fromJS(action.data)
    })
}

const taskReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SET_TASK_SELECTED_APP_ID:
            return setAppId(state, action);
        default:
            return state
    }
}

export default taskReducer