const productReducer = (state = {dataSource: [], page: 1, total: 0}, action) => {
    const { type, payload } = action;
    switch (type) {
        case "PRODUCT_LOADED":
            return {...state, dataSource: payload, page: 1, total: 0};
        default:
            return state;
    }
}
export default productReducer