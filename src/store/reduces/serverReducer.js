import {SET_SERVER_ID, LOAD_SERVER_LIST} from "../constant";


import { fromJS } from 'immutable'

const defaultState = fromJS({
    serverId: "",
    dataSource:[],
    page:1,
    pageSize:10,
    total:10
})

const setServerId = (state, action)=>{
    return state.merge({
        serverId: fromJS(action.data)
    })
}

const serverReducer = (state=defaultState, action)=>{
    const {type, payload} = action
    switch (type){
        case LOAD_SERVER_LIST:
            return {...state, dataSource: payload.data};
        case SET_SERVER_ID:
            return setServerId(state, action)
        default:
            return state;
    }
}

export default serverReducer;