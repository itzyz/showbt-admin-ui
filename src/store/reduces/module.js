const moduleReducer = (state = {dataSource: [], page: 1, pageSize: 10, total: 10}, action) => {
    const {type, payload} = action
    switch (type) {
        case "LOAD_MODULE":
            const data = payload.data
            return {...state, dataSource: data, page: 1, pageSize: 10, total: 10}
        default:
            return state
    }
}

export default moduleReducer;