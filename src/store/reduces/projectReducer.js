import {fromJS} from "immutable";
import {LOAD_PROJECT_LIST, SET_PROJECT_NAME} from "../constant";

const defaultState = fromJS({
    projectInfo: null,
    dataSource: [],
    page: 1,
    pageSize: 10,
    total: 10
})

const setProjectInfo = (state, action) => {
    return state.merge({
        projectInfo: fromJS(action.data)
    })
}

const projectReducer = (state = defaultState, action) => {
    switch (action.type) {
        case LOAD_PROJECT_LIST:
            const {payload} = action
            const data = payload.data
            return {
                ...state,
                dataSource: data.content,
                pageSize: data.size,
                total: data.totalElements
            }
        case SET_PROJECT_NAME:
            return setProjectInfo(state, action)
        default:
            return state
    }
}

export default projectReducer;