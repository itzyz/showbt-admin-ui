const MenuReducer = (state = {dataSource: []}, action) => {
    const {type, payload} = action
    switch (type) {
        case "LOAD_MENU":
            return {...state, dataSource: payload.data}
        default:
            return state
    }
}

export default MenuReducer;