import React, {lazy} from "react";
const Dashboard = lazy(()=>import( "../../pages/admin/dashboard/Dashboard"));
const ProjectList = lazy(()=>import( "../../pages/admin/gateway/ProjectList"));

const ProductList = lazy(()=>import("../../pages/admin/products/ProductList"))

class Test extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {current, name} = this.props
        console.log("--------Test.render-------------------", current, name, this.props.children)
        switch (current) {
            case "1001":
                return <Dashboard/>
            case "1002":
                return <ProductList />
            default:
                return <ProjectList/>
        }
    }
}

export default Test;