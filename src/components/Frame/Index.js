import React from 'react'
import {withRouter} from "react-router-dom"
import {Layout, Menu, Dropdown, Avatar, message} from 'antd';

// import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';

import * as Icon from '@ant-design/icons';
import {adminRoutes} from "../../routes/index"
import DownOutlined from "@ant-design/icons/lib/icons/DownOutlined";

import {loginOut} from "../../utils/auth";
import {createNavTreeData} from "../../utils/tools";
import logo from '@/images/logo.png'

const {Header, Sider} = Layout;
// const {TabPane} = Tabs
const initialPanes = [
    {title: '首页', content: "dashboard", key: "1001", path: '/index', closable: false,},
];

/**
 * https://www.cnblogs.com/leijuan/p/7857603.html
 * https://blog.csdn.net/crper/article/details/81977950?utm_medium=distribute.pc_aggpage_search_result.none-task-blog-2~all~sobaiduend~default-1-81977950.nonecase&utm_term=antd%E4%BA%8C%E7%BA%A7%E8%81%94%E5%8A%A8%E8%8F%9C%E5%8D%95&spm=1000.2123.3001.4430
 * **/
class Index extends React.Component {
    constructor(props) {
        super(props)
        this.newTabIndex = 0;
        this.state = {
            activeKey: initialPanes[0].key,
            panes: initialPanes,
            count: 0
        }
        this.urls = window.location.hash.replace('#', '');
    }


    componentDidMount = () => {
        console.log("===Frame>>=Index>>==componentDidMount===========")
        console.log(this.props.location.pathname);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log("===Frame>>=Index>>==componentDidUpdate===========")
        console.log(this.props.location.pathname);
    }

    onChange = activeKey => {
        this.setState({activeKey});
    };

    onEdit = (targetKey, action) => {
        this[action](targetKey);
    };

    add = () => {
        const {panes} = this.state;
        const activeKey = `newTab${this.newTabIndex++}`;
        const newPanes = [...panes];
        newPanes.push({title: 'New Tab', content: 'Content of new Tab', key: activeKey});
        this.setState({
            panes: newPanes,
            activeKey,
        });
    };

    addClick = (route) => {
        this.newTabIndex++
        const {panes} = this.state;
        const activeKey = route.key
        let isExist = false;
        panes.forEach((pane, i) => {
            if (pane.key === route.key) {
                isExist = true
            }
        });
        if (!isExist) {
            const newPanes = [...panes];
            newPanes.push({title: route.title, content: route.content, key: activeKey});
            this.setState({
                panes: newPanes,
                activeKey,
            });
        } else {
            this.setState({activeKey});
        }
    };

    remove = targetKey => {
        const {panes, activeKey} = this.state;
        let newActiveKey = activeKey;
        let lastIndex;
        panes.forEach((pane, i) => {
            if (pane.key === targetKey) {
                lastIndex = i - 1;
            }
        });
        const newPanes = panes.filter(pane => pane.key !== targetKey);
        if (newPanes.length && newActiveKey === targetKey) {
            if (lastIndex >= 0) {
                newActiveKey = newPanes[lastIndex].key;
            } else {
                newActiveKey = newPanes[0].key;
            }
        }
        this.setState({
            panes: newPanes,
            activeKey: newActiveKey,
        });
    };

    recursion = (props, menuList, parentPath = "") => {

        const {SubMenu} = Menu;
        return menuList.map(route => {
            if (route.children && route.children.length > 0) {
                return <SubMenu key={route.path} title={route.name} icon={route.icon && Icon[route.icon]?
                        React.createElement(
                            Icon[route.icon],
                            {
                                style: {fontSize: '16px', color: '#08c'}
                            }
                        ) : null
                }>
                    {
                        this.recursion(props, route.children, parentPath + route.path)
                    }
                </SubMenu>
            } else {
                if (route.path.startsWith("http")) {
                    return <Menu.Item key={route.path}>
                        {route.icon && Icon[route.icon]?
                            React.createElement(
                                Icon[route.icon],
                                {
                                    style: {fontSize: '16px', color: '#08c'}
                                }
                            ) : null
                        }
                        <a href={route.path} target="_blank" rel="noreferrer">{route.name}</a>
                    </Menu.Item>
                } else {
                    return <Menu.Item key={route.path}
                                      onClick={p => {
                                          props.history.push(parentPath + p.key)
                                          // this.addClick({
                                          //     title: route.name,
                                          //     key: route.id.toString(),
                                          //     path: parentPath + p.key,
                                          //     content: route.component
                                          // })
                                      }}
                    >
                        {route.icon && Icon[route.icon]?
                            React.createElement(
                                Icon[route.icon],
                                {
                                    style: {fontSize: '16px', color: '#08c'}
                                }
                            ) : null
                        }
                        {route.name}
                    </Menu.Item>
                }
            }
        })
    }

    render() {
        const menu = (
            <Menu onClick={(p) => {
                if (p.key === "loginOut") {
                    loginOut(this.props);
                    // this.props.history.push("/login");
                } else {
                    message.destroy();
                    message.info(p.key);
                }
            }}>
                <Menu.Item key="info">消息</Menu.Item>
                <Menu.Item key="setting">设置</Menu.Item>
                <Menu.Item key="loginOut">退出</Menu.Item>
            </Menu>
        )
        let menuTree = []
        createNavTreeData(menuTree, this.props.menuDataSource)
        // console.log("Frame-->>>menuTree>>>", menuTree)
        // const {panes, activeKey} = this.state;
        return (
            <Layout>
                <Header className="header"
                        style={{backgroundColor: '#11264f', display: "flex", justifyContent: "space-between"}}>
                    <div className="logo">
                         <img src={logo} alt="logo" style={{maxHeight:45}} />
                        <span size="5" style={{marginLeft:10,color: "#ffffff",fontSize:18}}>网关管理系统</span>
                    </div>
                    <Dropdown overlay={menu}>
                        <div>
                            <Avatar>U</Avatar>
                            <span style={{color: "#fff"}}>超级管理员</span>
                            <DownOutlined style={{color: "#fff"}}/>
                        </div>
                    </Dropdown>
                </Header>
                <Layout>
                    <Sider width={200} className="site-layout-background">
                        <Menu
                            mode="inline"
                            defaultSelectedKeys={['1']}
                            defaultOpenKeys={['sub1']}
                            style={{height: '100%', borderRight: 0}}
                        >
                            {adminRoutes.filter(route => route.isShow).map(route => {
                                return <Menu.Item key={route.path} onClick={p => {
                                    this.props.history.push(p.key)
                                    // this.addClick({
                                    //     title: route.title,
                                    //     key: route.key,
                                    //     path: route.path,
                                    //     content: route.component
                                    // })
                                }
                                }>
                                    {route.icon ?
                                        React.createElement(
                                            Icon[route.icon],
                                            {
                                                style: {fontSize: '16px', color: '#08c'}
                                            }
                                        ) : null
                                    }
                                    {route.title}
                                </Menu.Item>
                            })}
                            {
                                this.recursion(this.props, menuTree)
                            }
                        </Menu>
                    </Sider>
                    <Layout style={{padding: '16px'}}>
                        {this.props.children}
                    </Layout>
                </Layout>
            </Layout>
        )
    }
}


export default withRouter(Index)
