import Login from "../pages/Login";
import {lazy} from "react";
import PageNotFound from "../pages/PageNotFound";
import {getMyMenus} from "../utils/auth";

const Dashboard = lazy(() => import("../pages/admin/dashboard/Dashboard"))

export const mainRoutes = [
    {
        key: "2001",
        path: "/login",
        component: Login
    },
    {
        key: "2002",
        path: "/404",
        component: PageNotFound
    }
]

export const adminRoutes = [
    {
        key: "1001",
        path: "/admin/dashboard",
        component: Dashboard,
        isShow: true,
        title: "看板",
        icon: "HomeOutlined",
    },
]

/**
 * 将多级菜单扁平化成一级
 * 用于初始化时加载路由
 * @param result
 * @param menus
 * @returns {*[]}
 */
function remoteRoute(result = [], menus) {
    menus.forEach(m => {
        let obj = {
            key: m.id,
            path: m.path,
            component: lazy(() => import("../pages" + m.component)),
            exact: true,
            isShow: m.type === "0" ? true : false,
            icon: "AreaChartOutlined",
        }
        if (m.children && m.children.length > 0) {
            remoteRoute(result, m.children)
        }
        if (m.path && m.path !== "" && !m.path.startsWith("http") && (m.type === "0" || m.type === "2")) {
            result.push(obj)
        }
    })
    return result
}

export function allRoutes() {
    let menuData = getMyMenus();
    let remoteRouteArray = []
    remoteRoute(remoteRouteArray, menuData)
    return [
        ...adminRoutes,
        ...remoteRouteArray,
    ]
}