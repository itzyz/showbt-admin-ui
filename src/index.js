import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import "antd/dist/antd.css"

import {BrowserRouter as Router, Switch, Route, Redirect} from "react-router-dom"
import {mainRoutes} from './routes';
import store from "./store"
import {Provider} from "react-redux";


ReactDOM.render(
    <Provider store={store}>
        <Router>
                <Switch>
                    {/* 以admin开头的都用app组件进行渲染*/}
                    <Route path="/admin" render={routeProps => <App {...routeProps}/>}></Route>
                    {
                        mainRoutes.map(route => {
                            return <Route key={route.path} {...route}>
                            </Route>
                        })
                    }
                    <Redirect to="/admin" from="/"/>
                    <Redirect to="/404"/>
                </Switch>
        </Router>
    </Provider>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
