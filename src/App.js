import 'antd/dist/antd'

import {allRoutes} from './routes';
import Frame from './components/Frame/Index'
import {getMyMenus, isLogin} from "./utils/auth"
import React, {Suspense} from "react";
import {Redirect, Route, Switch} from "react-router-dom"


/**
 * 异步路由的解决方案
 * https://www.cnblogs.com/little-ab/articles/6956341.html
 *
 * @param props
 * @returns {*}
 * @constructor
 */

const App = () => {
    const menuDataSource = getMyMenus();
    return (
        isLogin() ?
            <Suspense fallback={<div>loading...</div>}>
                <Frame menuDataSource={menuDataSource}>
                    <Switch>
                        {
                            allRoutes().map(route => {
                                return <Route key={route.key}
                                              path={route.path}
                                              exact={route.exact}
                                              render={routeProps => {
                                                  return <route.component {...routeProps} />
                                              }}/>
                            })
                        }
                        <Redirect to={allRoutes()[0].path} from='/admin'/>
                        <Redirect to="/404"/>
                    </Switch>
                </Frame>
            </Suspense>
            : <Redirect to="/login"/>
    );
}
export default App;
