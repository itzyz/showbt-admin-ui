FROM nginx:latest

RUN mkdir -p /data/showbt-admin-ui

WORKDIR /data/showbt-admin-ui

ADD ./build/ .

