## 概述
这套UI純粹是为了学习一下react+antd,前端开发我是个小白，边抄边学边用，很多东西都是一知半解，希望找个项目来学习和实践，于是就有了这个开源项目。
后期我可能要解决两个问题，一个是权限管理问题，这个其实已经有很多开源项目可以用了；另外一个是解决微服务模式下的服务配置和管理，以及服务授权问题，
这类项目也有不少开源的，比如悟空、APISIX等

## 特点
1、本项目服务端采用spring-cloud微服务架构构建   
2、结合阿里的nacos，实现动态服务发现、配置和管理微服务  
3、采用sentinel，在网关层实现对接口监控与限流  
4、提供showbt-cloud-registry模块，用于将非spring-cloud框架开发的微服务自动注册到nacos中，
    这样第三方服务也可以通过spring-gateway进行服务转发  

## 学习笔记
该项目用于学习react + antd实现管理后台  
    [学习视频地址](https://b23.tv/4w012l)  
记录一下开发中遇到的问题与解决方法：  
    [React+Antd开发权限管理系统学习笔记（一）](https://mp.weixin.qq.com/s?__biz=MjM5ODkzNjA4Mw==&amp;mid=2458171406&amp;idx=1&amp;sn=c0993d18c1b6ac113c7da769612759be&amp;chksm=b1b3337286c4ba64bf4c0eb67e03fd2ffefed531fafcfcd4b45731aaa5b3214cd80a2d6427b1&token=1738535159&lang=zh_CN#rd)  
    [React+Antd开发权限管理系统学习笔记（二）](https://mp.weixin.qq.com/s?__biz=MjM5ODkzNjA4Mw==&amp;mid=2458171411&amp;idx=1&amp;sn=a29cfe3fd76968998d4563a81fe91a1b&amp;chksm=b1b3336f86c4ba794d2c4e711a3769bc2374021752058e175db18c00e0520c0d9a7aa9bdfb1d&token=1738535159&lang=zh_CN#rd)  
    [React+Antd开发权限管理系统学习笔记（三）](https://mp.weixin.qq.com/s?__biz=MjM5ODkzNjA4Mw==&amp;mid=2458171416&amp;idx=1&amp;sn=fb440cf777aecee568ed3e3b20854563&amp;chksm=b1b3336486c4ba72e3782661a2dcf580d3491e67db5b9fc1add124c2f2a2c975428a3f753c3b&token=1738535159&lang=zh_CN#rd)  
    [React+Antd开发权限管理系统学习笔记（四）](https://mp.weixin.qq.com/s?__biz=MjM5ODkzNjA4Mw==&amp;mid=2458171420&amp;idx=1&amp;sn=a654d4e0f9741ea827e8aff785c96f80&amp;chksm=b1b3336086c4ba7677bd1a81a112d37e22b4737e193ceed1570c685b71ebe646d32492789fe5&token=1738535159&lang=zh_CN#rd)  
    [React+Antd开发权限管理系统学习笔记（五）](https://mp.weixin.qq.com/s?__biz=MjM5ODkzNjA4Mw==&amp;mid=2458171425&amp;idx=1&amp;sn=d98f1137ca2b8483387f2855a49b7f94&amp;chksm=b1b3335d86c4ba4b391bbe1622f4351cb95d0930a175359fd8e6564c96f591e582ad5fa5b11d&token=1738535159&lang=zh_CN#rd)  
    [React+Antd开发权限管理系统学习笔记（六）](https://mp.weixin.qq.com/s?__biz=MjM5ODkzNjA4Mw==&amp;mid=2458171429&amp;idx=1&amp;sn=472ff9cc4e7a61abac3701bb27a09b8f&amp;chksm=b1b3335986c4ba4fc27e735c964b73da2f9b02ce8184d3b2eae415a29361f2b7a8f62e26f1e6&token=1738535159&lang=zh_CN#rd)

[微信公众号-技术分享专题](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MjM5ODkzNjA4Mw==&action=getalbum&album_id=2095129961593389056#wechat_redirect)
中可以查到以上文章

## 开发环境  
    node v12.16.3  

    npm 6.14.4  
    npm config get registry
        https://registry.npmjs.org/  
    
    yarn 1.22.10  
    yarn config get registry 
        https://registry.npm.taobao.org  

## 开发工具  
    visual studio code  
    web storm  

## 服务端仓库地址
[服务端](https://gitee.com/itzyz/showbt-admin-server)
## 社区仓库地址
[社区UI](https://gitee.com/itzyz/showbt-community-ui)


## 开发计划
    第一期：开发一套用户权限管理系统
![SHOWBT 图标](./doc/images/login.png)
![SHOWBT 图标](./doc/images/dashboard.png)
![SHOWBT 图标](./doc/images/menu.png)

    第二期：实现一套网关管理系统
![SHOWBT 图标](./doc/images/gateway.png)
    第三期：实现一个社区管理系统
![SHOWBT 图标](./doc/images/community.png)


## 本人联系方式
微信：   
![SHOWBT 微信](./doc/images/weixin.png)


微信公众号：  
![SHOWBT 微信](./doc/images/gongzhonghao.jpg)